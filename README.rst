The Last Gardener
=================

This is a Nim port of a Python game that was created for
`PyWeek 21 <https://pyweek.org/21/>`_.

The original Python code is located
`here <https://bitbucket.org/Jjp137/jjp-pyweek21/>`_.

Download
--------

The current version is 0.1.0.

Download the latest version of the game
`here <https://bitbucket.org/Jjp137/last-gardener/downloads/>`_.

Only builds for Linux 64-bit systems are currently provided. If you use
another operating system or a 32-bit Linux system, see below for build
instructions.

Once the game has been downloaded, decompress the tar archive, change
into the directory that was created, and then run the game with
``./gardener.sh``.

Build Instructions
------------------

If you are compiling from source, these dependencies are required:

* Nim 1.0.0+ or devel (1.0.4+ is recommended)
* nimgame2 devel (install with ``nimble install nimgame2@#devel``)
* SDL 2.0.7
* SDL_gfx 1.0.1
* SDL_image 2.0.2
* SDL_mixer 2.0.2
* SDL_ttf 2.0.14

Technically, you might be able to use older versions of SDL2 and have the
game work still, but the devel version of nimgame2 is a hard requirement.

Once all the dependencies have been installed, change directory to the
folder this README file is in, and compile with:

``nim c -o:gardener -d:release src/main.nim``

If the build is successful, start the game with:

``./gardener``

The Story
---------

Aliens have taken over Earth and wiped out most of humanity, but lawns still
need to be mowed. You happen to be the only gardener remaining. Use your
lawn-mowing drones and mow the lawns in this lawn-mowing bullet hell game!

How to Play
-----------

Menu / story controls:
- Arrow keys: Move cursor / Change option
- Space or Enter: Select option / Advance story
- Escape: Go back / Skip story

Gameplay controls:
- Arrow keys: Move
- Shift: Focus (slow down)
- Escape: Pause

During gameplay, move around with the arrow keys to mow the lawn while avoiding
bullets and enemies. Hold Shift to move slower and display your hitbox. You are
only hit by bullets if they collide with the hitbox. However, be careful not
to collide with enemies.

Mow the required percentage before you run out of lives or time in order to
win. If you have leftover time or lives, try going the extra mile and mow the
entire lawn. Those who want to test their skills should do so without getting
hit even once! You can retry a level as many times as you like.

There are three difficulty levels to choose from. To be honest, we're not
sure if beating all the levels on hard difficulty is possible :p
If you're not used to bullet hell games, we suggest playing on easy.

The game autosaves your progress in this folder. Delete save.dat before
launching the game if you wish to start over.

License
-------

The source code for The Last Gardener, which includes all files in the
'src' directory, is licensed under the BSD 3-clause license. The assets
are under varying licenses.

See LICENSE.txt for more details.

Credits
-------

First, thank you to those involved in organizing PyWeek 21 a few years
ago. This game and the Nim port would not have existed without the
original Python code and the event that inspired it.

The Nim port would not have been possible without the hard work of the
developers that worked on Nim, nimgame2, SDL2, and the libraries that
those projects depend on, so thank you for all your hard work!

Also, thank you to the following people whose resources we used in our game.
The names below may be real names or usernames on various websites. See
LICENSE.txt for more information.

* Fonts: Aydi Rainkarnichi, Sebastien Sanfilippo
* Graphics: Charlie, Chotkos, Daniel Cook, devnewton, Fanghong, Gumichan01,
  Jan Polák, Jordan Trudgett, Kenney, Master484, NASA, Neptuul,
  NicoleMarieProductions, pzUH
* Sounds: bart, fins, jedspear, jimhancock, mhtaylor67, milton., p0ss
* Music: Kevin MacLeod, Trevor Lentz

And finally, thank you for playing!

- Jjp137 and LegoBricker
