import math

import nimgame2/[assets, entity, input, texturegraphic, types]
import sdl2/sdl

import bullet, consts, data, sounds

const
  NormalSpeed = 5.0
  FocusSpeed = 2.0
  Size = 64.0  # Used for grass collision
  HitboxSize = 6.0  # Used for bullet collision

type
  Player* = ref object of Entity
    movedThisTick*: bool  # If there was movement this tick
    started*: bool  # If it moved at all during the level
    focused*: bool  # If the player is moving slowly by holding Shift
    active*: bool  # Set by the level to false when it ends in some way
    hitbox*: Hitbox
    lives*: Natural
    invulTime: Natural  # In ticks

  Hitbox = ref object of Entity
    player: Player  # Needed to access the player's fields and procs

proc newPlayer*(lives: Natural): Player =
  new(result)

  result.initEntity()
  result.graphic = data.sprites[SpinnerKey]
  # In case the player quit while the invul timer is still in effect
  data.sprites[SpinnerKey].alphaMod = 255

  result.pos = (400, 400)
  result.centrify()  # Once this is called, pos == location of the center
  result.layer = PlayerLayer

  result.tags.add(PlayerTag)
  result.collider = result.newBoxCollider((0.0, 0.0), (Size, Size))
  # Grass collisions are checked manually in a completely different way
  # so we don't include that tag
  result.collider.tags.add(EnemyTag)

  result.lives = lives
  result.active = true

  # Don't assign the hitbox a parent because otherwise the hitbox rotates too;
  # we'll move it manually along with the player
  new(result.hitbox)
  result.hitbox.initEntity()
  result.hitbox.player = result

  result.hitbox.graphic = data.sprites[HitboxKey]
  result.hitbox.pos = result.pos
  result.hitbox.centrify()
  result.hitbox.layer = HitboxLayer

  result.hitbox.tags.add(PlayerHitboxTag)
  result.hitbox.collider = result.newBoxCollider((0.0, 0.0),
                                                 (HitboxSize, HitboxSize))
  # We do it ourselves in the Level class for performance reasons
  result.hitbox.colliderEnabled = false
  result.hitbox.collider.tags.add(BulletTag)

  # Prepare the mowing sound loops
  sounds.initLoop(MowingLoopSoundKey)
  sounds.initLoop(MowingIdleSoundKey)

proc alive*(player: Player): bool =
  result = player.lives > 0

proc damage*(player: Player) =
  if not player.alive or not player.active or player.invulTime > 0:
    return

  dec player.lives
  sounds.playSound(PlayerHitSoundKey)

  if player.lives == 0:
    player.graphic = data.sprites[DeadSpinnerKey]

    sounds.pauseLoop(MowingLoopSoundKey)
    sounds.pauseLoop(MowingIdleSoundKey)
    sounds.playSound(PlayerDeathSoundKey)
  else:
    player.invulTime = 30
    # Technically, this applies to every entity using this graphic, but
    # there's only one player, so it's okay
    data.sprites[SpinnerKey].alphaMod = 128

proc rect*(player: Player): Rect =
  let topLeft = player.pos - player.center

  result = Rect(x: topLeft.x.cint, y: topLeft.y.cint,
                w: Size.cint, h: Size.cint)

proc rect*(hitbox: Hitbox): Rect =
  # The hitbox is 6x6 but the sprite is 10x10 so we cannot just subtract
  # the center from the current position
  let topLeft = (x: hitbox.pos.x - (HitboxSize / 2),
                 y: hitbox.pos.y - (HitBoxSize / 2))

  result = Rect(x: topLeft.x.cint, y: topLeft.y.cint,
                w: HitboxSize.cint, h: HitboxSize.cint)

method update*(player: Player, elapsed: float) =
  if not player.alive or not player.active:
    return

  # Don't update anything if no time passed (if the game is paused)
  if elapsed == 0.0:
    return

  if player.invulTime > 0:
    dec player.invulTime

    if player.invulTime == 0:
      sprites[SpinnerKey].alphaMod = 255

  let
    left = ScancodeLeft.down
    right = ScancodeRight.down
    up = ScancodeUp.down
    down = ScancodeDown.down
    focus = ScancodeLShift.down or ScancodeRShift.down

    baseSpeed = if focus: FocusSpeed else: NormalSpeed
    keysPressed = int(left) + int(right) + int(up) + int(down)
    # The result is 2 if even and 1 if odd
    someMath = ((keysPressed + 1) mod 2) + 1

    # LegoBricker once said:
    # The math behind this line is too long to write here.
    # Just trust that it works.
    # Ask me if you need an explanation
    finalSpeed = baseSpeed / sqrt(someMath.float)

  var
    dx = 0.0
    dy = 0.0

  if left: dx -= finalSpeed
  if right: dx += finalSpeed
  if up: dy -= finalSpeed  # y-axis is flipped (this differs from Pyglet!)
  if down: dy += finalSpeed

  player.movedThisTick = (dx != 0.0 or dy != 0.0)

  if not player.movedThisTick:
    # Pauses playing the mowing noise if you don't move this tick.
    # Then plays the idle sound.
    sounds.pauseLoop(MowingLoopSoundKey)

    if player.started:
      sounds.playSound(MowingIdleSoundKey)

  # "Start" the mower if this is the first movement
  if not player.started:
    player.started = player.movedThisTick

  let angleDelta =
    if player.movedThisTick:
      if focus: 6.0
      else: 10.0
    elif player.started: 2.0  # If the "mower" started up, rotate slightly
    else: 0.0

  player.rot = (player.rot + angleDelta) mod 360

  player.pos.x += dx
  player.pos.y += dy

  player.hitbox.pos = player.pos
  player.hitbox.visible = focus
  player.focused = focus

  # If you move, play the moving sound and pause the idle.
  if player.movedThisTick:
    sounds.playSound(MowingLoopSoundKey)
    sounds.pauseLoop(MowingIdleSoundKey)

method onCollide(player: Player, target: Entity) =
  # TODO: low priority -- use rect intersection for enemy collision
  if EnemyTag in target.tags:
    player.damage()

method onCollide(hitbox: Hitbox, target: Bullet) =
  assert BulletTag in target.tags

  var
    hitboxRect = hitbox.rect
    bulletRect = target.rect

  # In the original implementation, the player is only hit if the hitboxes
  # intersect, not just collide or touch edges
  if hasIntersection(addr(hitboxRect), addr(bulletRect)):
    hitbox.player.damage()
    target.dead = true
