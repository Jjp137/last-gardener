import tables

const
  NoImage* = ""

  IntroStoryKey* = "intro"
  HalfwayStoryKey* = "halfway"
  FinaleStoryKey* = "finale"
  EndingStoryKey* = "ending"

type
  Slide* = tuple[text: string, image: string]

let slides*: Table[string, seq[Slide]] = {
  IntroStoryKey: @[
    (text: "Once upon a time, the world was peaceful. " &
           "But one day, aliens invaded.",
     image: "story_1.png"),
    (text: "Most of humanity perished in the initial assault. " &
           "The survivors went into hiding.",
     image: "story_2.png"),
    (text: "You happen to be the last remaining gardener. " &
           "Lawns still need to be mowed, and the human resistance " &
           "has offered to pay for your services.",
     image: "story_3.png"),
    (text: "You still need money, so you accept the offer.",
     image: NoImage),
    (text: "Each day you send out a lawnmower drone " &
           "with the sole purpose of mowing the lawn. " &
           "Good luck!",
     image: "story_4.png"),
    (text: "Arrow keys control your movement. " &
           "Use the Shift key to focus and slow your speed. " &
           "When focused, your hitbox is shown. " &
           "The Escape key will pause the game.",
     image: "tutorial_1.png"),
    (text: "Mowing the lawn is done automatically. " &
           "Mow the required amount of the lawn to win, " &
           "even if the drone gets destroyed or runs out of fuel. " &
           "After all, the drones are expendable.",
     image: "tutorial_2.png"),
    (text: "Mow the entire lawn for a bonus! " &
           "Make sure to avoid anything bad, however. " &
           "Your drone can only take a few hits.",
     image: NoImage),
  ],
  HalfwayStoryKey: @[
    (text: "Things are getting pretty harrowing out there. " &
           "You considered quitting for a while, but you still " &
           "need the money.",
     image: "story_5.png"),
    (text: "Better get back to work now.",
     image: NoImage),
  ],
  FinaleStoryKey: @[
    (text: "The human resistance has asked you to mow the lawn of " &
           "the UN building. Their members will be inside " &
           "negotiating a peace treaty with the aliens.",
     image: "story_6.png"),
    (text: "The building seems to be guarded by some sort of massive " &
           "alien guardian. It will take your best " &
           "gardening skills to mow this lawn.",
     image: "story_7.png"),
  ],
  EndingStoryKey: @[
    (text: "After witnessing your gardening skills in practice at the UN, " &
           "the aliens have asked you to become their worldwide gardener " &
           "in exchange for peace on Earth. You accept.",
     image: "story_8.png"),
    (text: "As you leave for your new home, you look back on the " &
           "world you saved. They are grateful to you.",
     image: "story_9.png"),
    (text: "",  # Yes I'm aware this is stupidly hacky. -LegoBricker
     image: "story_10.png"),
  ]
}.toTable
