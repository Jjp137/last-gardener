import tables

import nimgame2/[types]

import consts, enemy, pattern, storydefs

const
  Always = {Easy, Normal, Hard}

  EasyOnly = {Easy,}
  NormalOnly = {Normal,}
  HardOnly = {Hard,}

  NotEasy = {Normal, Hard}

type
  SkillValues* = array[3, int]  # Natural is preferable but it's not inferred
  SkillSetting* = set[Difficulty]

  # There's probably a better way of doing this than duplicating most of
  # the fields, but that'll be for a future refactoring...

  PatternDef* = object
    skills*: SkillSetting  # Read by the Level object
    initialCooldown*: Natural  # In ticks
    bulletKey*: string
    activeAtStart*: bool
    onTime*: TimeProc
    timeArgs*: seq[int]
    onFire*: BulletProc
    fireArgs*: seq[float]
    toggleAt*: Natural  # Read by the Enemy object; percent to turn on or off

  EnemyDef* = object
    skills*: SkillSetting  # Read by the Level object
    key*: string
    startPos*: Coord
    speed*: float
    movement*: seq[MovementObj]
    patterns*: seq[PatternDef]

  LevelDef* = object
    number*: Natural
    lives*: SkillValues
    goalPct*: SkillValues
    time*: SkillValues  # In seconds
    storyBefore*: string
    storyClear*: string
    enemies*: seq[EnemyDef]

template to(x, y: float): MovementObj =
  MovementObj(kind: mkStraight, dest: (x, y))

template wait(t: Natural): MovementObj =
  MovementObj(kind: mkWait, ticks: t)

template wait(t: int): MovementObj =
  wait(t.Natural)

template sec(n: float): int = (n * GameFps).int

# In the future, put these in a file instead
# Maybe define a textual defintion for levels and parse it with npeg

let levelEntries*: Table[int, LevelDef] = {
  1: LevelDef(
    number: 1,
    lives: [3, 3, 2],
    goalPct: [75, 90, 90],
    time: [120, 90, 60],
    storyBefore: IntroStoryKey,
    storyClear: "",
    enemies: @[
      EnemyDef(
        skills: Always, key: RedAlienKey,
        startPos: (150, 100), speed: 3,

        movement: @[
          to(650, 100), to(650, 440), to(150, 440), to(150, 100)
        ],

        patterns: @[
          PatternDef(skills: EasyOnly, initialCooldown: sec(3),
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: sameTime, timeArgs: @[sec(2)],
            onFire: aimed, fireArgs: @[4.0], toggleAt: NoToggle
          ),
          PatternDef(skills: NormalOnly, initialCooldown: sec(1),
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: sameTime, timeArgs: @[sec(1.5)],
            onFire: nWay, fireArgs: @[4.0, 8.0], toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(0.5),
            bulletKey: BlueBulletKey, activeAtStart: true,
            onTime: sameTime, timeArgs: @[sec(0.5)],
            onFire: multiAimed, fireArgs: @[6.0, 3.0], toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(0.5),
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: sameTime, timeArgs: @[40],
            onFire: nWay, fireArgs: @[5.0, 16.0], toggleAt: NoToggle
          )
        ]
      )
    ]
  ),
  2: LevelDef(
    number: 2,
    lives: [3, 3, 2],
    goalPct: [75, 90, 90],
    time: [120, 90, 75],
    storyBefore: "",
    storyClear: "",
    enemies: @[
      EnemyDef(
        skills: Always, key: BlueAlienKey,
        startPos: (170, 60), speed: 4,

        movement: @[
          to(170, 480), wait(sec(2)), to(170, 60), wait(sec(2))
        ],

        patterns: @[
          PatternDef(skills: EasyOnly, initialCooldown: sec(1),
            bulletKey: BlueBulletKey, activeAtStart: true,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: direction, fireArgs: @[4.0, 0.0], toggleAt: NoToggle
          ),
          PatternDef(skills: NormalOnly, initialCooldown: sec(1),
            bulletKey: BlueBulletKey, activeAtStart: true,
            onTime: randomTime, timeArgs: @[sec(1), sec(1.5)],
            onFire: nWay, fireArgs: @[3.0, 8.0], toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(1),
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: aimed, fireArgs: @[4.0], toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(1),
            bulletKey: BlueBulletKey, activeAtStart: true,
            onTime: randomTime, timeArgs: @[sec(1), sec(1.5)],
            onFire: nWay, fireArgs: @[3.0, 16.0], toggleAt: NoToggle
          )
        ]
      ),
      EnemyDef(
        skills: Always, key: PurpleAlienKey,
        startPos: (630, 480), speed: 4,

        movement: @[
          to(630, 60), wait(sec(2)), to(630, 480), wait(sec(2))
        ],

        patterns: @[
          PatternDef(skills: EasyOnly, initialCooldown: sec(1),
            bulletKey: PurpleBulletKey, activeAtStart: true,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: direction, fireArgs: @[4.0, 180.0], toggleAt: NoToggle
          ),
          PatternDef(skills: NormalOnly, initialCooldown: sec(1),
            bulletKey: PurpleBulletKey, activeAtStart: true,
            onTime: randomTime, timeArgs: @[sec(1), sec(1.5)],
            onFire: nWay, fireArgs: @[3.0, 8.0], toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(1),
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: aimed, fireArgs: @[4.0], toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(1),
            bulletKey: PurpleBulletKey, activeAtStart: true,
            onTime: randomTime, timeArgs: @[sec(1), sec(1.5)],
            onFire: nWay, fireArgs: @[3.0, 16.0], toggleAt: NoToggle
          )
        ]
      ),
    ]
  ),
  3: LevelDef(
    number: 3,
    lives: [3, 3, 2],
    goalPct: [75, 85, 90],
    time: [120, 90, 90],
    storyBefore: "",
    storyClear: "",
    enemies: @[
      EnemyDef(
        skills: Always, key: RedAlienKey,
        startPos: (400, 60), speed: 2,

        movement: @[
          to(60, 270), wait(sec(1)), to(400, 480), wait(sec(1)),
          to(740, 270), wait(sec(1)), to(400, 60), wait(sec(1))
        ],

        patterns: @[
          PatternDef(skills: EasyOnly, initialCooldown: sec(1),
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: nRandom, fireArgs: @[1.0, 3.0, 6.0], toggleAt: NoToggle
          ),
          PatternDef(skills: EasyOnly, initialCooldown: 1,
            bulletKey: BlueBulletKey, activeAtStart: false,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: nWay, fireArgs: @[3.0, 8.0], toggleAt: 50
          ),
          PatternDef(skills: NormalOnly, initialCooldown: sec(1),
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: burstTime, timeArgs: @[10, 5, 80],
            onFire: incNWay, fireArgs: @[3.0, 8.0, 2.0, 1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: NormalOnly, initialCooldown: 1,
            bulletKey: BlueBulletKey, activeAtStart: false,
            onTime: randomTime, timeArgs: @[50, 70],
            onFire: nRandom, fireArgs: @[2.0, 4.0, 20.0], toggleAt: 50
          ),
          PatternDef(skills: HardOnly, initialCooldown: 80,
            bulletKey: PurpleBulletKey, activeAtStart: true,
            onTime: burstTime, timeArgs: @[8, 5, sec(1)],
            onFire: incNWay, fireArgs: @[3.0, 8.0, 4.0, -1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: 1,
            bulletKey: BlueBulletKey, activeAtStart: false,
            onTime: randomTime, timeArgs: @[90, 110],
            onFire: nWay, fireArgs: @[1.0, 32.0], toggleAt: 30
          ),
          PatternDef(skills: HardOnly, initialCooldown: 1,
            bulletKey: CyanBulletKey, activeAtStart: false,
            onTime: burstTime, timeArgs: @[2, 10, 40],
            onFire: aimed, fireArgs: @[5.0], toggleAt: 60
          )
        ]
      )
    ]
  ),
  4: LevelDef(
    number: 4,
    lives: [3, 3, 2],
    goalPct: [70, 80, 90],
    time: [120, 90, 90],
    storyBefore: "",
    storyClear: "",
    enemies: @[
      EnemyDef(
        skills: Always, key: BlueAlienKey,
        startPos: (400, 100), speed: 3,

        movement: @[
          to(700, 100), wait(sec(1)), to(700, 440), wait(sec(1)),
          to(100, 440), wait(sec(1)), to(400, 270), wait(sec(2)),
          to(100, 100), wait(sec(1))
        ],

        patterns: @[
          PatternDef(skills: EasyOnly, initialCooldown: sec(1),
            bulletKey: BlueBulletKey, activeAtStart: true,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: incNWay, fireArgs: @[3.0, 8.0, 4.0, 1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: EasyOnly, initialCooldown: 1,
            bulletKey: PurpleBulletKey, activeAtStart: false,
            onTime: burstTime, timeArgs: @[10, 4, sec(1)],
            onFire: nRandom, fireArgs: @[2.0, 3.0, 1.0], toggleAt: 50
          ),
          PatternDef(skills: NormalOnly, initialCooldown: sec(1),
            bulletKey: BlueBulletKey, activeAtStart: true,
            onTime: randomTime, timeArgs: @[sec(1)+10, sec(1.5)],
            onFire: incNWay, fireArgs: @[2.0, 16.0, 2.0, 1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: NormalOnly, initialCooldown: 1,
            bulletKey: PurpleBulletKey, activeAtStart: false,
            onTime: sameTime, timeArgs: @[10],
            onFire: nRandom, fireArgs: @[2.0, 3.0, 1.0], toggleAt: 50
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(1),
            bulletKey: BlueBulletKey, activeAtStart: true,
            onTime: randomTime, timeArgs: @[sec(1)+20, sec(1.5)+10],
            onFire: circles, fireArgs: @[1.0, 16.0, 4.0, 0.0, 10.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: 1,
            bulletKey: PurpleBulletKey, activeAtStart: false,
            onTime: sameTime, timeArgs: @[5],
            onFire: spiral, fireArgs: @[3.0, 24.0, 1.0, 1.0],
            toggleAt: 40
          )
        ]
      ),
      EnemyDef(
        skills: Always, key: RedAlienKey,
        startPos: (400, 100), speed: 2,

        movement: @[
          to(100, 100), wait(sec(1)), to(100, 440), wait(sec(1)),
          to(700, 440), wait(sec(1)), to(700, 100), wait(sec(1))
        ],

        patterns: @[
          PatternDef(skills: EasyOnly, initialCooldown: sec(1),
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: aimed, fireArgs: @[4.0], toggleAt: NoToggle
          ),
          PatternDef(skills: EasyOnly, initialCooldown: 1,
            bulletKey: PurpleBulletKey, activeAtStart: false,
            onTime: burstTime, timeArgs: @[10, 4, sec(1)],
            onFire: nRandom, fireArgs: @[2.0, 3.0, 1.0], toggleAt: 50
          ),
          PatternDef(skills: NormalOnly, initialCooldown: sec(1),
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: burstTime, timeArgs: @[10, 5, sec(1)],
            onFire: aimed, fireArgs: @[4.0], toggleAt: NoToggle
          ),
          PatternDef(skills: NormalOnly, initialCooldown: 1,
            bulletKey: PurpleBulletKey, activeAtStart: false,
            onTime: sameTime, timeArgs: @[10],
            onFire: nRandom, fireArgs: @[2.0, 3.0, 1.0], toggleAt: 50
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(0.5),
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: burstTime, timeArgs: @[5, 6, sec(1)],
            onFire: multiAimed, fireArgs: @[5.0, 4.0, 3.0, 2.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: 1,
            bulletKey: PurpleBulletKey, activeAtStart: false,
            onTime: sameTime, timeArgs: @[5],
            onFire: spiral, fireArgs: @[3.0, 24.0, 1.0, -1.0],
            toggleAt: 40
          )
        ]
      ),
    ]
  ),
  5: LevelDef(
    number: 5,
    lives: [3, 3, 2],
    goalPct: [70, 80, 90],
    time: [120, 90, 90],
    storyBefore: "",
    storyClear: HalfwayStoryKey,
    enemies: @[
      EnemyDef(
        skills: Always, key: RedAlienKey,
        startPos: (100, 100), speed: 3,

        movement: @[
          to(700, 100), wait(sec(1)), to(100, 440), wait(sec(1)),
          to(700, 440), wait(sec(1)), to(100, 100), wait(sec(1))
        ],

        patterns: @[
          PatternDef(skills: EasyOnly, initialCooldown: sec(1),
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: burstTime, timeArgs: @[40, 2, sec(1)+20],
            onFire: incNWay, fireArgs: @[2.0, 12.0, 2.0, -1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: EasyOnly, initialCooldown: 1,
            bulletKey: BlueBulletKey, activeAtStart: false,
            onTime: sameTime, timeArgs: @[10],
            onFire: spiral, fireArgs: @[3.0, 12.0, 2.0, 1.0],
            toggleAt: 50
          ),
          PatternDef(skills: NormalOnly, initialCooldown: 1,
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: burstTime, timeArgs: @[30, 2, sec(1)],
            onFire: nWay, fireArgs: @[2.0, 12.0], toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: 1,
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: burstTime, timeArgs: @[20, 3, sec(1)],
            onFire: nWay, fireArgs: @[2.0, 16.0], toggleAt: NoToggle
          ),
        ]
      ),
      EnemyDef(
        skills: NotEasy, key: RedAlienKey,
        startPos: (400, 40), speed: 3,

        movement: @[
          to(400, 500), to(400, 40)
        ],

        patterns: @[
          PatternDef(skills: NormalOnly, initialCooldown: 1,
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: sameTime, timeArgs: @[sec(0.5)],
            onFire: nWay, fireArgs: @[4.0, 2.0], toggleAt: 50
          ),
          PatternDef(skills: NormalOnly, initialCooldown: 1,
            bulletKey: BlueBulletKey, activeAtStart: false,
            onTime: burstTime, timeArgs: @[10, 16, sec(1)],
            onFire: spiral, fireArgs: @[3.0, 16.0, 3.0, 1.0],
            toggleAt: 50
          ),
          PatternDef(skills: HardOnly, initialCooldown: 1,
            bulletKey: BlueBulletKey, activeAtStart: true,
            onTime: sameTime, timeArgs: @[20],
            onFire: spiral, fireArgs: @[3.0, 32.0, 4.0, 1.0],
            toggleAt: 30
          ),
          PatternDef(skills: HardOnly, initialCooldown: 1,
            bulletKey: BlueBulletKey, activeAtStart: false,
            onTime: sameTime, timeArgs: @[10],
            onFire: spiral, fireArgs: @[3.0, 32.0, 4.0, 1.0],
            toggleAt: 30
          ),
          PatternDef(skills: HardOnly, initialCooldown: 1,
            bulletKey: PurpleBulletKey, activeAtStart: false,
            onTime: burstTime, timeArgs: @[5, 5, sec(1)],
            onFire: multiAimed, fireArgs: @[4.0, 3.0, 2.0],
            toggleAt: 60
          )
        ]
      ),
    ]
  ),
  6: LevelDef(
    number: 6,
    lives: [4, 3, 2],
    goalPct: [70, 80, 90],
    time: [120, 90, 90],
    storyBefore: "",
    storyClear: "",
    enemies: @[
      EnemyDef(
        skills: Always, key: BlueAlienKey,
        startPos: (760, 270), speed: 2,

        movement: @[
          to(40, 270), wait(sec(1)), to(760, 270), wait(sec(1))
        ],

        patterns: @[
          PatternDef(skills: EasyOnly, initialCooldown: 1,
            bulletKey: BlueBigBulletKey, activeAtStart: false,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: aimed, fireArgs: @[3.0], toggleAt: 5
          ),
          PatternDef(skills: EasyOnly, initialCooldown: 1,
            bulletKey: BlueBulletKey, activeAtStart: false,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: incNWay, fireArgs: @[3.0, 8.0, 2.0, 1.0],
            toggleAt: 30
          ),
          PatternDef(skills: NormalOnly, initialCooldown: 1,
            bulletKey: BlueBigBulletKey, activeAtStart: false,
            onTime: sameTime, timeArgs: @[sec(0.5)+10],
            onFire: aimed, fireArgs: @[3.0], toggleAt: 5
          ),
          PatternDef(skills: NormalOnly, initialCooldown: 1,
            bulletKey: BlueBulletKey, activeAtStart: false,
            onTime: randomTime, timeArgs: @[50, 70],
            onFire: incNWay, fireArgs: @[3.0, 16.0, 2.0, 1.0],
            toggleAt: 30
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(1),
            bulletKey: BlueBulletKey, activeAtStart: true,
            onTime: randomTime, timeArgs: @[sec(1)+20, sec(2)],
            onFire: incNWay, fireArgs: @[1.0, 24.0, 3.0, 1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: 1,
            bulletKey: CyanBigBulletKey, activeAtStart: false,
            onTime: randomTime, timeArgs: @[sec(1), sec(1)+40],
            onFire: incNWay, fireArgs: @[2.5, 6.0, 3.0, 1.0],
            toggleAt: 30
          ),
          PatternDef(skills: HardOnly, initialCooldown: 1,
            bulletKey: YellowBigBulletKey, activeAtStart: false,
            onTime: sameTime, timeArgs: @[sec(2)],
            onFire: aimed, fireArgs: @[2.0],
            toggleAt: 60
          ),
        ]
      ),
      EnemyDef(
        skills: Always, key: PurpleAlienKey,
        startPos: (400, 40), speed: 2,

        movement: @[
          to(400, 500), wait(sec(1)), to(400, 40), wait(sec(1))
        ],

        patterns: @[
          PatternDef(skills: EasyOnly, initialCooldown: 1,
            bulletKey: PurpleBigBulletKey, activeAtStart: false,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: aimed, fireArgs: @[3.0], toggleAt: 5
          ),
          PatternDef(skills: EasyOnly, initialCooldown: 1,
            bulletKey: PurpleBulletKey, activeAtStart: false,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: incNWay, fireArgs: @[3.0, 8.0, 2.0, 1.0],
            toggleAt: 30
          ),
          PatternDef(skills: NormalOnly, initialCooldown: 1,
            bulletKey: PurpleBigBulletKey, activeAtStart: false,
            onTime: sameTime, timeArgs: @[sec(0.5)+10],
            onFire: aimed, fireArgs: @[3.0], toggleAt: 5
          ),
          PatternDef(skills: NormalOnly, initialCooldown: 1,
            bulletKey: PurpleBulletKey, activeAtStart: false,
            onTime: randomTime, timeArgs: @[50, 70],
            onFire: incNWay, fireArgs: @[3.0, 16.0, 2.0, 1.0],
            toggleAt: 30
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(1),
            bulletKey: PurpleBulletKey, activeAtStart: true,
            onTime: randomTime, timeArgs: @[sec(1)+20, sec(2)],
            onFire: incNWay, fireArgs: @[1.0, 24.0, 3.0, 1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: 1,
            bulletKey: CyanBigBulletKey, activeAtStart: false,
            onTime: randomTime, timeArgs: @[sec(1), sec(1)+40],
            onFire: incNWay, fireArgs: @[2.5, 6.0, 3.0, 1.0],
            toggleAt: 30
          ),
          PatternDef(skills: HardOnly, initialCooldown: 1,
            bulletKey: YellowBigBulletKey, activeAtStart: false,
            onTime: sameTime, timeArgs: @[sec(2)],
            onFire: aimed, fireArgs: @[2.0],
            toggleAt: 60
          ),
        ]
      ),
    ]
  ),
  7: LevelDef(
    number: 7,
    lives: [4, 3, 2],
    goalPct: [70, 80, 90],
    time: [120, 90, 90],
    storyBefore: "",
    storyClear: "",
    enemies: @[
      EnemyDef(
        skills: Always, key: RedAlienKey,
        startPos: (60, 60), speed: 3,

        movement: @[
          to(740, 60), wait(sec(1)), to(400, 480), wait(sec(1)),
          to(60, 60), wait(sec(1))
        ],

        patterns: @[
          PatternDef(skills: EasyOnly, initialCooldown: sec(1),
            bulletKey: YellowBigBulletKey, activeAtStart: true,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: incNWay, fireArgs: @[1.0, 4.0, 4.0, 1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: EasyOnly, initialCooldown: 1,
            bulletKey: PurpleBulletKey, activeAtStart: false,
            onTime: sameTime, timeArgs: @[sec(0.5)],
            onFire: aimed, fireArgs: @[4.0], toggleAt: 40
          ),
          PatternDef(skills: NormalOnly, initialCooldown: sec(1),
            bulletKey: YellowBigBulletKey, activeAtStart: true,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: incNWay, fireArgs: @[1.0, 7.0, 4.0, 1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: NormalOnly, initialCooldown: 1,
            bulletKey: PurpleBulletKey, activeAtStart: false,
            onTime: burstTime, timeArgs: @[2, 10, sec(1)],
            onFire: nRandom, fireArgs: @[3.0, 4.0, 1.0],
            toggleAt: 40
          ),
          PatternDef(skills: NormalOnly, initialCooldown: 1,
            bulletKey: PurpleBulletKey, activeAtStart: false,
            onTime: burstTime, timeArgs: @[3, 5, sec(1)],
            onFire: aimed, fireArgs: @[4.0], toggleAt: 40
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(1),
            bulletKey: YellowBigBulletKey, activeAtStart: true,
            onTime: randomTime, timeArgs: @[50, 70],
            onFire: incNWay, fireArgs: @[1.0, 9.0, 4.0, 1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: 1,
            bulletKey: PurpleBulletKey, activeAtStart: false,
            onTime: burstTime, timeArgs: @[3, 5, sec(1)],
            onFire: spreadAimed, fireArgs: @[3.0, 5.0, 90.0],
            toggleAt: 40
          ),
          PatternDef(skills: HardOnly, initialCooldown: 1,
            bulletKey: BlueBulletKey, activeAtStart: false,
            onTime: randomTime, timeArgs: @[25, 35],
            onFire: nWay, fireArgs: @[3.0, 8.0], toggleAt: 40
          ),
        ]
      ),
      EnemyDef(
        skills: Always, key: RedAlienKey,
        startPos: (60, 480), speed: 3,

        movement: @[
          to(400, 60), wait(sec(1)), to(700, 480), wait(sec(1)),
          to(60, 480), wait(sec(1))
        ],

        patterns: @[
          PatternDef(skills: EasyOnly, initialCooldown: sec(1),
            bulletKey: YellowBigBulletKey, activeAtStart: true,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: incNWay, fireArgs: @[1.0, 4.0, 3.0, -1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: EasyOnly, initialCooldown: 1,
            bulletKey: PurpleBulletKey, activeAtStart: false,
            onTime: sameTime, timeArgs: @[sec(0.5)],
            onFire: aimed, fireArgs: @[4.0], toggleAt: 40
          ),
          PatternDef(skills: NormalOnly, initialCooldown: sec(1),
            bulletKey: YellowBigBulletKey, activeAtStart: true,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: incNWay, fireArgs: @[1.0, 7.0, 4.0, 1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: NormalOnly, initialCooldown: sec(1),
            bulletKey: YellowBigBulletKey, activeAtStart: true,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: incNWay, fireArgs: @[1.0, 7.0, 4.0, 1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: NormalOnly, initialCooldown: 1,
            bulletKey: PurpleBulletKey, activeAtStart: false,
            onTime: burstTime, timeArgs: @[2, 10, sec(1)],
            onFire: nRandom, fireArgs: @[3.0, 4.0, 1.0],
            toggleAt: 40
          ),
          PatternDef(skills: NormalOnly, initialCooldown: 1,
            bulletKey: PurpleBulletKey, activeAtStart: false,
            onTime: burstTime, timeArgs: @[3, 5, sec(1)],
            onFire: aimed, fireArgs: @[4.0], toggleAt: 40
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(1),
            bulletKey: YellowBigBulletKey, activeAtStart: true,
            onTime: randomTime, timeArgs: @[50, 70],
            onFire: incNWay, fireArgs: @[1.0, 9.0, 4.0, 1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: 1,
            bulletKey: PurpleBulletKey, activeAtStart: false,
            onTime: burstTime, timeArgs: @[3, 5, sec(1)],
            onFire: spreadAimed, fireArgs: @[3.0, 5.0, 90.0],
            toggleAt: 40
          ),
          PatternDef(skills: HardOnly, initialCooldown: 1,
            bulletKey: BlueBulletKey, activeAtStart: false,
            onTime: randomTime, timeArgs: @[25, 35],
            onFire: nWay, fireArgs: @[3.0, 8.0], toggleAt: 40
          ),
        ]
      ),
    ]
  ),
  8: LevelDef(
    number: 8,
    lives: [4, 3, 2],
    goalPct: [75, 80, 90],
    time: [180, 120, 120],
    storyBefore: "",
    storyClear: "",
    enemies: @[
      EnemyDef(
        skills: Always, key: RedAlienKey,
        startPos: (740, 60), speed: 5,

        movement: @[
          to(60, 60), to(60, 480), to(740, 480), to(740, 165), to(230, 165),
          to(230, 375), to(570, 375), to(570, 270), to(400, 270), wait(sec(5)),
          to(570, 270), to(570, 375), to(230, 375), to(230, 165), to(740, 165),
          to(740, 480), to(60, 480), to(60, 60), to(740, 60), to(740, 480),
          to(60, 480), to(60, 165), to(570, 165), to(570, 375), to(230, 375),
          to(230, 270), to(400, 270), wait(sec(5)), to(230, 270), to(230, 375),
          to(570, 375), to(570, 165), to(60, 165), to(60, 480), to(740, 480),
          to(740, 60)
        ],

        patterns: @[
          PatternDef(skills: EasyOnly, initialCooldown: sec(1),
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: incNWay, fireArgs: @[2.0, 16.0, 4.0, -1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: EasyOnly, initialCooldown: 1,
            bulletKey: RedBigBulletKey, activeAtStart: false,
            onTime: randomTime, timeArgs: @[50, 70],
            onFire: nRandom, fireArgs: @[2.0, 3.0, 8.0],
            toggleAt: 40
          ),
          PatternDef(skills: NormalOnly, initialCooldown: sec(1),
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: sameTime, timeArgs: @[40],
            onFire: incNWay, fireArgs: @[2.0, 16.0, 4.0, -1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: NormalOnly, initialCooldown: 1,
            bulletKey: RedBigBulletKey, activeAtStart: false,
            onTime: randomTime, timeArgs: @[50, 70],
            onFire: incNWay, fireArgs: @[2.0, 8.0, 3.0, 1.0],
            toggleAt: 30
          ),
          PatternDef(skills: NormalOnly, initialCooldown: 1,
            bulletKey: BlueBigBulletKey, activeAtStart: false,
            onTime: randomTime, timeArgs: @[25, 35],
            onFire: incNWay, fireArgs: @[0.5, 1.0, 4.0, 1.0],
            toggleAt: 60
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(1),
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: sameTime, timeArgs: @[20],
            onFire: incNWay, fireArgs: @[2.0, 24.0, 4.0, -1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: 1,
            bulletKey: RedBigBulletKey, activeAtStart: false,
            onTime: randomTime, timeArgs: @[25, 35],
            onFire: spiral, fireArgs: @[2.0, 16.0, 6.0, 1.0],
            toggleAt: 30
          ),
          PatternDef(skills: HardOnly, initialCooldown: 1,
            bulletKey: BlueBulletKey, activeAtStart: false,
            onTime: burstTime, timeArgs: @[2, 10, 60],
            onFire: aimed, fireArgs: @[3.0],
            toggleAt: 60
          ),
        ]
      )
    ]
  ),
  9: LevelDef(
    number: 9,
    lives: [4, 3, 2],
    goalPct: [50, 60, 90],
    time: [120, 120, 120],
    storyBefore: "",
    storyClear: "",
    enemies: @[
      EnemyDef(
        skills: Always, key: RedAlienKey,
        startPos: (30, 510), speed: 3,

        movement: @[
          to(30, 30), to(770, 30), to(770, 510), to(30, 510)
        ],

        patterns: @[
          PatternDef(skills: EasyOnly, initialCooldown: sec(1),
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[sec(1), sec(90)],
            onFire: nWay, fireArgs: @[3.0, 4.0], toggleAt: NoToggle
          ),
          PatternDef(skills: EasyOnly, initialCooldown: 1,
            bulletKey: YellowBigBulletKey, activeAtStart: false,
            onTime: sameTime, timeArgs: @[sec(1.5)],
            onFire: aimed, fireArgs: @[3.0], toggleAt: 30
          ),
          PatternDef(skills: NormalOnly, initialCooldown: sec(1),
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[sec(1), sec(90)],
            onFire: nWay, fireArgs: @[3.0, 6.0], toggleAt: NoToggle
          ),
          PatternDef(skills: NormalOnly, initialCooldown: 1,
            bulletKey: YellowBigBulletKey, activeAtStart: false,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: aimed, fireArgs: @[3.0], toggleAt: 30
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(1),
            bulletKey: YellowBigBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[sec(1), sec(90)],
            onFire: nWay, fireArgs: @[3.0, 8.0], toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(10),
            bulletKey: PurpleBigBulletKey, activeAtStart: true,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: aimed, fireArgs: @[3.0], toggleAt: NoToggle
          ),
        ]
      ),
      EnemyDef(
        skills: Always, key: BlueAlienKey,
        startPos: (30, 30), speed: 3,

        movement: @[
          to(770, 30), to(770, 510), to(30, 510), to(30, 30)
        ],

        patterns: @[
          PatternDef(skills: EasyOnly, initialCooldown: sec(1),
            bulletKey: BlueBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[sec(1), sec(90)],
            onFire: nWay, fireArgs: @[3.0, 4.0], toggleAt: NoToggle
          ),
          PatternDef(skills: EasyOnly, initialCooldown: 1,
            bulletKey: BlueBigBulletKey, activeAtStart: false,
            onTime: sameTime, timeArgs: @[sec(1.5)],
            onFire: aimed, fireArgs: @[3.0], toggleAt: 30
          ),
          PatternDef(skills: NormalOnly, initialCooldown: sec(1),
            bulletKey: BlueBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[sec(1), sec(90)],
            onFire: nWay, fireArgs: @[3.0, 6.0], toggleAt: NoToggle
          ),
          PatternDef(skills: NormalOnly, initialCooldown: 1,
            bulletKey: BlueBigBulletKey, activeAtStart: false,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: aimed, fireArgs: @[3.0], toggleAt: 30
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(1),
            bulletKey: BlueBigBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[sec(1), sec(90)],
            onFire: nWay, fireArgs: @[3.0, 8.0], toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(10),
            bulletKey: PurpleBigBulletKey, activeAtStart: true,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: aimed, fireArgs: @[3.0], toggleAt: NoToggle
          ),
        ]
      ),
      EnemyDef(
        skills: Always, key: RedAlienKey,
        startPos: (770, 30), speed: 3,

        movement: @[
          to(770, 510), to(30, 510), to(30, 30), to(770, 30),
        ],

        patterns: @[
          PatternDef(skills: EasyOnly, initialCooldown: sec(1),
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[sec(1), sec(90)],
            onFire: nWay, fireArgs: @[3.0, 4.0], toggleAt: NoToggle
          ),
          PatternDef(skills: EasyOnly, initialCooldown: 1,
            bulletKey: YellowBigBulletKey, activeAtStart: false,
            onTime: sameTime, timeArgs: @[sec(1.5)],
            onFire: aimed, fireArgs: @[3.0], toggleAt: 30
          ),
          PatternDef(skills: NormalOnly, initialCooldown: sec(1),
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[sec(1), sec(90)],
            onFire: nWay, fireArgs: @[3.0, 6.0], toggleAt: NoToggle
          ),
          PatternDef(skills: NormalOnly, initialCooldown: 1,
            bulletKey: YellowBigBulletKey, activeAtStart: false,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: aimed, fireArgs: @[3.0], toggleAt: 30
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(1),
            bulletKey: YellowBigBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[sec(1), sec(90)],
            onFire: nWay, fireArgs: @[3.0, 8.0], toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(10),
            bulletKey: PurpleBigBulletKey, activeAtStart: true,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: aimed, fireArgs: @[3.0], toggleAt: NoToggle
          ),
        ]
      ),
      EnemyDef(
        skills: Always, key: BlueAlienKey,
        startPos: (770, 510), speed: 3,

        movement: @[
          to(30, 510), to(30, 30), to(770, 30), to(770, 510)
        ],

        patterns: @[
          PatternDef(skills: EasyOnly, initialCooldown: sec(1),
            bulletKey: BlueBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[sec(1), sec(90)],
            onFire: nWay, fireArgs: @[3.0, 4.0], toggleAt: NoToggle
          ),
          PatternDef(skills: EasyOnly, initialCooldown: 1,
            bulletKey: BlueBigBulletKey, activeAtStart: false,
            onTime: sameTime, timeArgs: @[sec(1.5)],
            onFire: aimed, fireArgs: @[3.0], toggleAt: 30
          ),
          PatternDef(skills: NormalOnly, initialCooldown: sec(1),
            bulletKey: BlueBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[sec(1), sec(90)],
            onFire: nWay, fireArgs: @[3.0, 6.0], toggleAt: NoToggle
          ),
          PatternDef(skills: NormalOnly, initialCooldown: 1,
            bulletKey: BlueBigBulletKey, activeAtStart: false,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: aimed, fireArgs: @[3.0], toggleAt: 30
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(1),
            bulletKey: BlueBigBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[sec(1), sec(90)],
            onFire: nWay, fireArgs: @[3.0, 8.0], toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(10),
            bulletKey: PurpleBigBulletKey, activeAtStart: true,
            onTime: sameTime, timeArgs: @[sec(1)],
            onFire: aimed, fireArgs: @[3.0], toggleAt: NoToggle
          ),
        ]
      )
    ]
  ),
  10: LevelDef(
    number: 10,
    lives: [5, 5, 5],
    goalPct: [100, 100, 100],
    time: [1000, 1000, 1000],
    storyBefore: FinaleStoryKey,
    storyClear: EndingStoryKey,
    enemies: @[
      EnemyDef(
        skills: Always, key: BossAlienKey,
        startPos: (400, 0), speed: 4,

        movement: @[
          # Have the boss fly out after two minutes
          to(400, 100), wait(sec(120)), to(400, -300), wait(sec(1000))
        ],

        patterns: @[
          PatternDef(skills: EasyOnly, initialCooldown: sec(1),
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: untilBurst, timeArgs: @[20, 3, sec(1), sec(30)],
            onFire: incNWay, fireArgs: @[3.0, 24.0, 4.0, 1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: EasyOnly, initialCooldown: sec(11),
            bulletKey: RedBigBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[sec(1.5), sec(20)],
            onFire: aimed, fireArgs: @[4.0], toggleAt: NoToggle
          ),
          PatternDef(skills: EasyOnly, initialCooldown: sec(31),
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[5, sec(40)],
            onFire: spiral, fireArgs: @[3.0, 24.0, 2.0, 1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: EasyOnly, initialCooldown: sec(41),
            bulletKey: BlueBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[5, sec(30)],
            onFire: spiral, fireArgs: @[3.0, 32.0, 2.0, -1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: EasyOnly, initialCooldown: sec(51),
            bulletKey: RedBigBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[sec(2), sec(20)],
            onFire: aimed, fireArgs: @[2.0], toggleAt: NoToggle
          ),
          PatternDef(skills: EasyOnly, initialCooldown: sec(51),
            bulletKey: PurpleBigBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[sec(1.5), sec(20)],
            onFire: incNWay, fireArgs: @[3.0, 8.0, 8.0, 1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: EasyOnly, initialCooldown: sec(71),
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[5, sec(50)],
            onFire: nRandom, fireArgs: @[3.0, 4.0, 3.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: EasyOnly, initialCooldown: sec(86),
            bulletKey: BlueBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[5, sec(35)],
            onFire: nRandom, fireArgs: @[3.0, 4.0, 2.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: EasyOnly, initialCooldown: sec(101),
            bulletKey: PurpleBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[5, sec(20)],
            onFire: nRandom, fireArgs: @[3.0, 4.0, 2.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: NormalOnly, initialCooldown: sec(1),
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: untilBurst, timeArgs: @[20, 5, sec(1), sec(40)],
            onFire: incNWay, fireArgs: @[3.0, 32.0, 4.0, 1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: NormalOnly, initialCooldown: sec(11),
            bulletKey: BlueBulletKey, activeAtStart: true,
            onTime: untilBurst, timeArgs: @[20, 5, sec(1), sec(30)],
            onFire: incNWay, fireArgs: @[3.0, 24.0, 4.0, -1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: NormalOnly, initialCooldown: sec(21),
            bulletKey: CyanBigBulletKey, activeAtStart: true,
            onTime: untilBurst, timeArgs: @[10, 3, sec(1.5), sec(20)],
            onFire: aimed, fireArgs: @[2.0], toggleAt: NoToggle
          ),
          PatternDef(skills: NormalOnly, initialCooldown: sec(41),
            bulletKey: PurpleBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[sec(2), sec(29)],
            onFire: circles, fireArgs: @[3.0, 24.0, 6.0, 0.0, 60.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: NormalOnly, initialCooldown: sec(41),
            bulletKey: YellowBigBulletKey, activeAtStart: true,
            onTime: untilBurst, timeArgs: @[5, 3, sec(1), sec(29)],
            onFire: incNWay, fireArgs: @[2.0, 8.0, 8.0, 1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: NormalOnly, initialCooldown: sec(71),
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[5, sec(50)],
            onFire: nRandom, fireArgs: @[3.0, 4.0, 5.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: NormalOnly, initialCooldown: sec(71),
            bulletKey: BlueBigBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[20, sec(50)],
            onFire: spiral, fireArgs: @[3.0, 5.0, 4.0, -1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: NormalOnly, initialCooldown: sec(81),
            bulletKey: RedBulletKey, activeAtStart: true,
            onTime: untilBurst, timeArgs: @[2, 5, sec(1), sec(40)],
            onFire: spreadAimed, fireArgs: @[3.0, 5.0, 90.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: NormalOnly, initialCooldown: sec(91)+40,
            bulletKey: BlueBulletKey, activeAtStart: true,
            onTime: untilBurst, timeArgs: @[2, 5, sec(1), sec(30)],
            onFire: spreadAimed, fireArgs: @[3.0, 4.0, 90.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: NormalOnly, initialCooldown: sec(101),
            bulletKey: PurpleBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[5, sec(20)],
            onFire: nRandom, fireArgs: @[3.0, 4.0, 5.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(1),
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[20, sec(40)],
            onFire: incNWay, fireArgs: @[4.0, 32.0, 8.0, 1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(11),
            bulletKey: BlueBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[20, sec(30)],
            onFire: incNWay, fireArgs: @[4.0, 24.0, 6.0, -1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(21),
            bulletKey: RedBigBulletKey, activeAtStart: true,
            onTime: untilBurst, timeArgs: @[10, 3, sec(1.5), sec(50)],
            onFire: multiAimed, fireArgs: @[4.0, 3.0, 2.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(41),
            bulletKey: PurpleBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[sec(2), sec(29)],
            onFire: circles, fireArgs: @[4.0, 24.0, 8.0, 0.0, 60.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(41),
            bulletKey: YellowBigBulletKey, activeAtStart: true,
            onTime: untilBurst, timeArgs: @[5, 3, sec(1), sec(29)],
            onFire: incNWay, fireArgs: @[3.0, 8.0, 8.0, -1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(51),
            bulletKey: CyanBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[sec(1.5), sec(65)],
            onFire: incNWay, fireArgs: @[1.0, 32.0, 2.0, 1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(71),
            bulletKey: YellowBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[6, sec(50)],
            onFire: nRandom, fireArgs: @[3.0, 4.0, 3.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(71),
            bulletKey: BlueBigBulletKey, activeAtStart: true,
            onTime: untilTime, timeArgs: @[20, sec(50)],
            onFire: spiral, fireArgs: @[3.0, 10.0, 8.0, -1.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(81),
            bulletKey: RedBulletKey, activeAtStart: true,
            onTime: untilBurst, timeArgs: @[2, 5, sec(1), sec(40)],
            onFire: spreadAimed, fireArgs: @[3.0, 7.0, 90.0],
            toggleAt: NoToggle
          ),
          PatternDef(skills: HardOnly, initialCooldown: sec(91),
            bulletKey: BlueBulletKey, activeAtStart: true,
            onTime: untilBurst, timeArgs: @[2, 5, sec(1), sec(30)],
            onFire: spreadAimed, fireArgs: @[3.0, 6.0, 90.0],
            toggleAt: NoToggle
          ),
        ]
      )
    ]
  ),
}.toTable

