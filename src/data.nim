import os, tables

import nimgame2/[assets, audio, texturegraphic, truetypefont]

import consts

var
  sprites*: Assets[TextureGraphic]
  ui*: Assets[TextureGraphic]
  sounds*: Assets[Sound]
  music*: Assets[Music]
  mainFont*: Table[int, TrueTypeFont]

proc makeSound(filename: string): Sound =
  echo "Loading: " & filename
  return newSound(filename)

proc makeMusic(filename: string): Music =
  echo "Loading: " & filename
  return newMusic(filename)

proc makeTexture(filename: string): TextureGraphic =
  echo "Loading: " & filename
  return newTextureGraphic(filename)

proc loadData*() =
  sprites = newAssets[TextureGraphic]("data" / "sprites", makeTexture)
  ui = newAssets[TextureGraphic]("data" / "ui", makeTexture)
  music = newAssets[Music]("data" / "music", makeMusic)
  sounds = newAssets[Sound]("data" / "sounds", makeSound)

  const sizes = [SmallFontSize, NormalFontSize,
                 MenuItemFontSize, PopupTitleFontSize]
  for size in sizes:
    let newFont = newTrueTypeFont()
    if newFont.load("data" / "fonts" / "bagnard_sans.ttf", size):
      mainFont[size] = newFont

proc freeData*() =
  for sprite in sprites.values:
    sprite.free()

  for ui in ui.values:
    ui.free()

  # Segfaults because when the game loop ends, TTF_Quit() is called, which
  # causes any subsequent calls to TTF_CloseFont() to cause a segfault
  # A similar issue happens with sounds
  when false:
    for font in mainFont.values:
      font.free()

    for sound in sounds.values:
      sound.free()
