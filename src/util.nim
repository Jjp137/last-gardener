import math

import nimgame2/[types, utils]

proc getAngle*(origin, point: Coord): float =
  ## Gets the angle in radians between two points.
  ##
  ## The angle is measured along the x-axis, and it is between -pi/2 and pi/2.
  ## It is negated to account for the inverted y-axis.

  # nimgame2's direction proc adds 90 degrees so that the result
  # is between 0 and pi, but we rely on it being between -pi/2 and pi/2
  # so adjust accordingly (thanks LegoBricker for this comment)
  # Also, the direction proc uses -atan2 so the y-axis is already accounted for
  result = rad(direction(origin, point) - 90)

proc getVelocity*(speed, radians: float): Coord =
  ## Gets the velocity given a speed and an angle in radians.
  ##
  ## This proc assumes that the y-axis is inverted and that radians is
  ## either a value from the getAngle proc or has been negated.

  # No need to flip the y value here
  result = (x: speed * cos(radians), y: speed * sin(radians))

proc getDirection*(start, finish: Coord): Coord =
  ## Given two points, gets the direction from ``start`` to ``finish``
  ## as a vector.
  ##
  ## The value of each component is either -1, 0, or 1.

  # Negate the value of y since the y-axis is inverted
  result = (x: sgn(finish.x - start.x).float,
            y: -sgn(finish.y - start.y).float)
