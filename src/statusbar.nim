import strformat, tables

import nimgame2/[assets, draw, entity, textgraphic, texturegraphic, types]

import consts, data, level

const
  TopMargin = 8.0
  BottomMargin = StatusBarHeight - 8.0
  LeftMargin = 10.0
  RightMargin = GameWidth - 10.0
  MidLineY = 36.0

type
  StatusBar* = ref object of Entity
    level: Level
    progress: TextGraphic
    progressNums: TextGraphic
    progressColor: Color
    time: TextGraphic
    lives: TextGraphic
    levelNum: TextGraphic
    skill: TextGraphic

proc newStatusBar*(level: Level): StatusBar =
  new(result)

  result.initEntity()
  result.level = level

  result.graphic = ui[StatusBarKey]
  result.pos = (0, LevelHeight)
  result.layer = StatusBarLayer

  # A note in general: Pyglet 1.1+ uses 96 DPI, but SDL2_ttf uses 72 DPI
  # Ideally, the DPI should be a parameter, but that isn't possible
  # without using FreeType directly or patching SDL2_ttf, and neither
  # of those options are appealing

  result.progress = newTextGraphic(mainFont[NormalFontSize])
  result.progress.setText("Progress:")

  result.progressNums = newTextGraphic(mainFont[NormalFontSize])
  result.progressNums.setText("")

  result.time = newTextGraphic(mainFont[NormalFontSize])
  result.time.setText("Time left: 0.00")

  result.lives = newTextGraphic(mainFont[NormalFontSize])
  result.lives.setText("Lives: ")

  result.levelNum = newTextGraphic(mainFont[NormalFontSize])
  result.levelNum.setText(fmt"Level {result.level.number}")

  const skillNames = ["Easy", "Normal", "Hard"]
  result.skill = newTextGraphic(mainFont[NormalFontSize])
  result.skill.setText(fmt"{skillNames[result.level.skill.int]}")

method update*(status: StatusBar, elapsed: float) =
  status.updateEntity(elapsed)

  let level = status.level  # For readability

  let newTime = fmt"Time left: {level.timeLeft.float / 60.0:.2f}"
  status.time.setText(newTime)

  status.time.color =
    if level.timeLeft == 0: ColorRed
    elif level.timeLeft < 10 * GameFps: ColorYellow
    else: ColorWhite

  status.lives.color =
    if level.playerLives == 0: ColorRed
    elif level.playerLives == 1: ColorYellow
    else: ColorWhite

  let newProgress = fmt"{level.donePct:.2f} / {level.goalPct:.2f}"
  status.progressNums.setText(newProgress)

  status.progressColor =
    if level.state == PerfectClear: ColorGold
    elif level.state == GrassClear: ColorSilver
    elif level.donePct >= level.goalPct: ColorLightBlue
    else: ColorRed

method render*(status: StatusBar) =
  status.renderEntity()

  # Draw the remaining time and the "Lives:" text
  status.time.draw(pos = (LeftMargin, LevelHeight + TopMargin))
  status.lives.draw(pos = (220.0, LevelHeight + TopMargin))

  # Draw the heart icons that represent lives
  let
    firstX = 260.0  # Original impl: 290.0
    heartSpacing = 2.0
    heart = sprites[HeartKey]
    heartWidth = heart.dim.w.float

  for i in 1..status.level.playerLives:
    let
      heartX = firstX + i.float * (heartSpacing + heartWidth)
      heartY = LevelHeight + TopMargin
    heart.draw(pos = (heartX, heartY))

  # Calculate various coordinates for the progress bar
  let
    progressWidth = 375.0
    progressHeight = 18.0
    innerWidth = progressWidth - 2  # Subtract the left and right pixels

    topY = GameHeight - 24.0
    bottomY = topY + progressHeight
    leftX = 95.0  # Original impl: 114.0
    rightX = leftX + progressWidth

    barWidth = (status.level.donePct / 100.0) * innerWidth
    # Add 1 to exclude the left side of the outline
    barEndX = (leftX + 1) + barWidth
    cutoffX = (leftX + 1) + (status.level.goalPct / 100.0) * innerWidth

  # Draw the "Progress:" text
  status.progress.draw(pos = (LeftMargin, LevelHeight + MidLineY))

  # Draw the outline first
  discard rect((leftX, topY), (rightX, bottomY), ColorWhite)

  # Draw the actual bar next, then draw the cutoff over it
  # Start the actual bar within the outline
  # For some reason, one pixel is added to barEndX and bottomY, so compensate
  if barEndX > leftX + 2:  # Prevent overriding the left edge of the outline
    let barColor = status.progressColor
    discard box((leftX + 1, topY + 1), (barEndX - 1, bottomY - 2), barColor)

  discard line((cutoffX, topY), (cutoffX, bottomY - 1), ColorWhite)

  # Draw the percentage numbers (x in original impl: 500.0)
  status.progressNums.draw(pos = (481.0, LevelHeight + MidLineY))

  # Draw the level information, and anchor each line on the top-right
  status.levelNum.draw(pos = (RightMargin, LevelHeight + TopMargin),
                       center = (status.levelNum.w.float, 0.0))
  status.skill.draw(pos = (RightMargin, LevelHeight + MidLineY),
                    center = (status.skill.w.float, 0.0))
