import tables, sets

import nimgame2/[assets, audio]

import data

const
  Play = true
  Pause = false

var soundDebug* = false

# Keep track of what sound used a particular channel when debugging
var channels = initTable[string, int]()

# Pending actions (either play or pause)
var pending = initTable[string, bool]()

# Sounds that are meant to be looped
var looping = initHashSet[string]()

var
  # Currently playing music
  curMusic: Music = nil
  soundEnabled = true
  musicEnabled = true

proc removeLoop*(key: string) =
  let sound = sounds[key]
  sound.stop()

  looping.excl(key)

proc initLoop*(key: string) =
  if key in looping:
    removeLoop(key)

  looping.incl(key)

proc pauseLoop*(key: string) =
  pending[key] = Pause

proc playSound*(key: string) =
  ## Plays the sound with the given key.
  ##
  ## If the sound is a loop, it either starts the loop or resumes it if
  ## it is paused.
  pending[key] = Play

proc toggleSound*() =
  soundEnabled = not soundEnabled

  if not soundEnabled:
    soundStop()

proc soundOn*(): bool =
  result = soundEnabled

proc playMusic*(key: string, restart = false) =
  ## Plays music, or resumes paused music.
  if not musicEnabled:
    return

  let mus = music[key]

  if curMusic != nil and curMusic == mus:
    if musicPlaying() and musicPaused() and not restart:
      musicResume()
      return
    elif not restart:
      return

  setMusicVolume(8)

  mus.play(-1)  # Infinite loops
  curMusic = mus

proc pauseMusic*() =
  if curMusic != nil and musicPlaying() and not musicPaused():
    musicPause()

proc toggleMusic*() =
  musicEnabled = not musicEnabled

  if not musicEnabled:
    musicStop()
    curMusic = nil

proc musicOn*(): bool =
  result = musicEnabled

proc tick*() =
  if soundEnabled:
    # Handle any pending actions
    for key, action in pending:
      let sound = sounds[key]

      # Handle sounds that are designed to loop in a different way
      if key in looping:
        if sound.playing and not sound.paused and action == Pause:
          sound.pause()
        elif sound.playing and sound.paused and action == Play:
          sound.resume()
        elif not sound.playing and action == Play:
          sound.volume = 8
          channels[key] = sound.play(-1)
      # Otherwise, just get the sound and play it
      elif action == Play:
        sound.volume = 8
        channels[key] = sound.play()

  # Clear the table of all pending actions
  pending.clear()

  if soundDebug:
    echo channels
