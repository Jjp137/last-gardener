import math, tables

import nimgame2/[assets, types, utils]

import bullet, data, util

type
  StateData = TableRef[string, float]

  TimeProc* = proc(state: StateData, args: openArray[int]): Natural
  BulletProc* = proc(bulletKey: string; origin, target: Coord,
                     state: StateData, args: openArray[float]): seq[Bullet]

  Pattern* = ref object of RootObj
    active*: bool

    cooldown: Natural  # In ticks
    bulletKey: string

    # Proc to call when the cooldown runs out; returns a new cooldown
    onTime: TimeProc
    timeArgs: seq[int]

    # Proc to call to generate bullets
    onFire: BulletProc
    fireArgs: seq[float]

    state: StateData

proc newPattern*(cooldown: Natural, bulletKey: string, active: bool,
                 onTime: TimeProc, timeArgs: seq[int],
                 onFire: BulletProc, fireargs: seq[float]): Pattern =
  new(result)

  result.active = active

  result.cooldown = cooldown
  result.bulletKey = bulletKey

  result.onTime = onTime
  result.timeArgs = timeArgs

  result.onFire = onFire
  result.fireArgs = fireArgs

  result.state = newTable[string, float]()

proc generateBullets*(pattern: Pattern; origin, target: Coord): seq[Bullet] =
  if not pattern.active:
    return

  dec pattern.cooldown

  if pattern.cooldown == 0:
    result = pattern.onFire(pattern.bulletKey, origin, target,
                            pattern.state, pattern.fireArgs)
    pattern.cooldown = pattern.onTime(pattern.state, pattern.timeArgs)
  else:
    result = @[]

template createBullet(bulletKey: string; origin, vel: Coord): Bullet =
  let hitbox = bulletHitboxes[bulletKey]
  newBullet(sprites[bulletKey], hitbox, origin, vel)

proc sameTime*(state: StateData, args: openArray[int]): Natural =
  assert args.len == 1

  result = args[0]

proc randomTime*(state: StateData, args: openArray[int]): Natural =
  assert args.len == 2

  result = rand(args[0]..args[1])

# TODO: in the future, these procs should be generated via a macro
# that generates static and random variations for each proc

# Fire 'count' bullets with 'time' between them, then wait 'delay'
proc burstTime*(state: StateData, args: openArray[int]): Natural =
  assert args.len == 3

  let
    time = args[0]
    count = args[1]
    delay = args[2]
  var
    cur = state.mGetOrPut("time_count", 0).int

  inc cur

  # If that was the x-th time we fired, wait for the delay
  if cur == count:
    cur = 0
    result = delay
  else:
    result = time

  state["time_count"] = cur.float

# Continue a pattern for 'timeout' seconds
proc untilTime*(state: StateData, args: openArray[int]): Natural =
  assert args.len == 2

  let
    time = args[0]
    timeout = args[1]
  var
    cur = state.mGetOrPut("timeAccum", 0).int

  cur += time
  state["timeAccum"] = cur.float

  if cur > timeout:
    return 10 ^ 7  # Some really high number
  else:
    return time

proc untilBurst*(state: StateData, args: openArray[int]): Natural =
  assert args.len == 4

  let
    time = args[0]
    count = args[1]
    delay = args[2]
    timeout = args[3]
  var
    cur = state.mGetOrPut("time_count", 0).int
    accum = state.mGetOrPut("time_accum", 0).int

  inc cur

  # If that was the x-th time we fired, wait for the delay
  if cur == count:
    cur = 0
    result = delay
  else:
    result = time

  accum += result

  if accum > timeout:
    result = 10 ^ 7  # Some really high number

  state["time_accum"] = accum.float
  state["time_count"] = cur.float

proc nWay*(bulletKey: string; origin, target: Coord, state: StateData,
           args: openArray[float]): seq[Bullet] =
  assert args.len == 2

  let
    speed = args[0]
    count = args[1]
    angleInc = PI * 2.0 / count

  for i in 0..<count.int:
    let vel = getVelocity(speed, angleInc * i.float)
    result.add(createBullet(bulletKey, origin, vel))

# Same as nWay, but offset the angle every time
proc incNWay*(bulletKey: string; origin, target: Coord, state: StateData,
              args: openArray[float]): seq[Bullet] =
  assert args.len == 4

  let
    speed = args[0]
    count = args[1]
    increment = args[2]
    sign = args[3]
  var
    cur = state.mGetOrPut("fire_inc", 0)

  let
    angleInc = PI * 2 / count  # Angle between bullets
    sliceInc = angleInc / increment  # Angle between bullets divided into parts
    startAngle = sliceInc * cur.float  # Angle to use as offset

  for i in 0..<count.int:
    let vel = getVelocity(speed, startAngle + angleInc * i.float)
    result.add(createBullet(bulletKey, origin, vel))

  cur += sign * 1.0
  state["fire_inc"] = cur

# Create 'circles' circles with 'count' bullets, starting at 'degrees'
proc circles*(bulletKey: string, origin, target: Coord, state: StateData,
              args: openArray[float]): seq[Bullet] =
  assert args.len == 5

  let
    speed = args[0]
    count = args[1]
    circles = args[2]
    degrees = args[3]
    radius = args[4]

    baseAngle = rad(degrees.Angle)
    angleInc = PI * 2 / circles
    circleInc = PI * 2 / count

  for i in 0..<circles.int:
    let
      angle = baseAngle + i.float * angleInc
      x = origin.x + (radius * cos(angle))
      y = origin.y + (radius * sin(angle))
      circlePos = (x: x, y: y)

    for j in 0..<count.int:
      let vel = getVelocity(speed, circleInc * j.float)
      result.add(createBullet(bulletKey, circlePos, vel))

# Create a spiral of bullets; basically same as incNWay except one at a time
# Starting angle is random
proc spiral*(bulletKey: string, origin, target: Coord, state: StateData,
             args: openArray[float]): seq[Bullet] =
  assert args.len == 4

  let
    speed = args[0]
    count = args[1]
    spokes = args[2]
    sign = args[3]

    start = state.mGetOrPut("start", rand(0.0..PI*2))
  var
    cur = state.mGetOrPut("fireInc", 0)

  let
    angleInc = PI * 2 / count
    angle = angleInc * cur
    spokeInc = PI * 2 / spokes

  for spoke in 0..<spokes.int:
    let vel = getVelocity(speed, start + angle + spokeInc * spoke.float)

    result.add(createBullet(bulletKey, origin, vel))

  cur += sign * 1.0
  state["fireInc"] = cur
  state["start"] = start

# Fire a single bullet at a given direction
proc direction*(bulletKey: string, origin, target: Coord, state: StateData,
                args: openArray[float]): seq[Bullet] =
  assert args.len == 2

  let
    speed = args[0]
    degrees = args[1]
    angle = rad(degrees.Angle)
    vel = getVelocity(speed, angle)

  result.add(createBullet(bulletKey, origin, vel))

proc aimed*(bulletKey: string; origin, target: Coord, state: StateData,
            args: openArray[float]): seq[Bullet] =
  assert args.len == 1

  let
    speed = args[0]
    angle = getAngle(origin, target)
    vel = getVelocity(speed, angle)

  result.add(createBullet(bulletKey, origin, vel))

proc spreadAimed*(bulletKey: string; origin, target: Coord, state: StateData,
                  args: openArray[float]): seq[Bullet] =
  assert args.len == 3

  let
    speed = args[0]
    count = args[1]
    degrees = args[2]

    radians = rad(degrees)

    playerAngle = getAngle(origin, target)
    angleInc = radians / (count - 1)
    startAngle = playerAngle - (radians / 2)

  for i in 0..<count.int:
    let vel = getVelocity(speed, startAngle + angleInc * i.float)

    result.add(createBullet(bulletKey, origin, vel))

# For firing multiple aimed bullets at different speeds
proc multiAimed*(bulletKey: string; origin, target: Coord, state: StateData,
                 args: openArray[float]): seq[Bullet] =
  assert args.len > 1

  let angle = getAngle(origin, target)

  for speed in args:
    let vel = getVelocity(speed, angle)
    result.add(createBullet(bulletKey, origin, vel))

# Fire n bullets randomly
proc nRandom*(bulletKey: string; origin, target: Coord, state: StateData,
              args: openArray[float]): seq[Bullet] =
  assert args.len == 3

  let
    minSpeed = args[0]
    maxSpeed = args[1]
    count = args[2]

  for i in 0..<count.int:
    let
      angle = rand(0.0 .. PI * 2.0)
      vel = getVelocity(rand(minSpeed..maxSpeed), angle)

    result.add(createBullet(bulletKey, origin, vel))

# For testing purposes
proc spammy*(bulletKey: string; origin, target: Coord, state: StateData,
             args: openArray[float]): seq[Bullet] =
  for _ in 0..rand(10..50):
    let vel = (rand(-9.0..9.0), rand(-9.0..9.0))

    result.add(createBullet(bulletKey, origin, vel))
