import nimgame2/[nimgame, settings, utils]

import consts, data, profile, scenes, sounds

proc main() =
  game = newGame()
  let success = game.init(GameWidth, GameHeight, GameTitle)

  if not success:
    echo "Initializing game failed."
    return

  # The default is 10ms for some reason
  updateInterval = secToMs(1.0 / GameFps)

  loadData()

  profile.current = initProfile()

  # Music and sound default to on
  if not profile.current.musicOn:
    sounds.toggleMusic()
  if not profile.current.soundOn:
    sounds.toggleSound()

  game.scene = newMainMenuScene()
  game.run()

  profile.current.saveProfile()

  freeData()

when isMainModule:
  main()
