import tables

import nimgame2/[assets, entity, texturegraphic, types]

import bullet, consts, pattern, player, sounds, util

const
  HitboxDim: Dim = (30, 44)
  BossDim: Dim = (132, 77)

type
  PatternToggle* = tuple[pattern: Pattern, togglePercent: Natural]

  MovementKind* = enum
    mkStraight,
    mkWait

  MovementObj* = object
    case kind*: MovementKind
    of mkStraight:
      dest*: Coord
    of mkWait:
      ticks*: Natural

  Enemy* = ref object of Entity
    speed: float
    direction: Coord

    patterns: seq[PatternToggle]

    movement: seq[MovementObj]
    movePos: Natural
    pauseTime: Natural

    newBullets*: seq[Bullet]
    levelDonePct*: float  # Set by the level; crappy hack to avoid cyclic imports

    player: Player  # Crappy hack to avoid cyclic imports

let enemyHitboxes*: Table[string, Dim] = {
  RedAlienKey: HitboxDim,
  BlueAlienKey: HitboxDim,
  CyanAlienKey: HitboxDim,
  GreenAlienKey: HitboxDim,
  PurpleAlienKey: HitboxDim,
  BossAlienKey: BossDim
}.toTable

proc calcVel(enemy: Enemy) =
  let item = enemy.movement[enemy.movePos]
  if item.kind == mkWait:
    enemy.pauseTime = item.ticks
    enemy.vel = (0, 0)
  else:
    let radians = getAngle(enemy.pos, item.dest)
    enemy.vel = getVelocity(enemy.speed, radians)
    # We don't just figure out the direction from the velocity because
    # cos(pi/2) ends up being some really small number instead of zero,
    # which actually breaks the enemy movement code
    enemy.direction = getDirection(enemy.pos, item.dest)

proc newEnemy*(graphic: TextureGraphic, pos: Coord, hitbox: Dim,
               speed: float, movement: seq[MovementObj],
               patterns: seq[PatternToggle], player: Player): Enemy =
  new(result)
  result.initEntity()

  result.graphic = graphic
  result.pos = pos
  result.centrify()
  result.layer = EnemyLayer
  result.tags.add(EnemyTag)

  result.collider = result.newBoxCollider((0.0, 0.0), hitbox)
  result.collider.tags.add(PlayerTag)

  result.speed = speed

  result.movePos = 0
  result.pauseTime = 0

  result.movement = movement
  result.patterns = patterns

  result.levelDonePct = 0.0
  result.player = player

  if result.movement.len > 0:
    result.calcVel()

proc move(enemy: Enemy) =
  if enemy.movement.len == 0:
    return

  if enemy.pauseTime > 0:
    dec enemy.pauseTime
    if enemy.pauseTime == 0:
      enemy.movePos = (enemy.movePos + 1) mod enemy.movement.len
      enemy.calcVel()

    return

  let
    dest = enemy.movement[enemy.movePos].dest
    newPos = enemy.pos + enemy.vel

    curDir = enemy.direction
    destDir = getDirection(newPos, dest)

    passedXAxis = abs(destDir.x - curDir.x) > 0
    passedYAxis = abs(destDir.y - curDir.y) > 0
    noXMovement = (curDir.x == 0 and destDir.x == 0)
    noYMovement = (curDir.y == 0 and destDir.y == 0)

  # It is determined that the enemy reached the current destination if for
  # each axis, either there is no movement on that axis, or the direction
  # from the new position to the destination is different from the direction
  # from the old position to the destination
  if (passedXAxis or noXMovement) and (passedYAxis or noYMovement):
    enemy.pos = dest
    enemy.movePos = (enemy.movePos + 1) mod enemy.movement.len
    enemy.calcVel()
    # FIXME: add the leftover here (inherited from the original impl)
  else:
    enemy.pos = newPos

proc fire(enemy: Enemy) =
  for i, item in mpairs(enemy.patterns):
    let togglePct = item.togglePercent.float
    if togglePct != NoToggle.float and enemy.levelDonePct >= togglePct:
      item.pattern.active = not item.pattern.active
      # Don't toggle again
      item.togglePercent = NoToggle

    for bullet in item.pattern.generateBullets(enemy.pos, enemy.player.pos):
      sounds.playSound(EnemyFireSoundKey)
      enemy.newBullets.add(bullet)

method update*(enemy: Enemy, elapsed: float) =
  # Don't update anything if no time passed (if the game is paused)
  if elapsed == 0.0:
    return

  enemy.move()
  enemy.fire()
