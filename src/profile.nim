import marshal, os, streams, tables

import consts

type
  Profile* = ref object of RootObj
    clearData*: Table[string, int]
    lastPlayed*: tuple[number: Natural, skill: Difficulty]
    musicOn*: bool
    soundOn*: bool

# This is global because the alternative is to pass it into every scene
var current*: Profile

proc initProfile*(): Profile =
  let savePath = getAppDir() / "save.dat"
  new(result)

  if existsFile(savePath):
    try:
      let fin = openFileStream(savePath)
      load(fin, result)
      fin.close()
    except IOError:
      echo "Can't load profile!"
  else:
    result.clearData = initTable[string, int]()
    result.clearData["unlocked"] = 1

    result.lastPlayed = (number: 1.Natural, skill: Normal)

    result.musicOn = true
    result.soundOn = true

proc saveProfile*(profile: Profile) =
  let savePath = getAppDir() / "save.dat"

  try:
    let fout = openFileStream(savePath, mode = fmWrite)
    store(fout, profile)
    fout.close()
  except IOError:
    echo "Can't save profile!"

proc updateResult*(profile: Profile, level: Natural, skill: Difficulty,
                   outcome: LevelState) =
  assert outcome in [Passed, GrassClear, PerfectClear]

  let key = "level_" & $level & "_" & $skill.int

  if key notin profile.clearData:
    profile.clearData[key] = outcome.int
  else:
    let prev = profile.clearData[key].LevelState

    if prev == PerfectClear:
      discard
    elif prev == GrassClear and outcome == PerfectClear:
      profile.clearData[key] = outcome.int
    elif prev == Passed and outcome in [GrassClear, PerfectClear]:
      profile.clearData[key] = outcome.int

  let unlocked = profile.clearData["unlocked"]
  if unlocked <= level:
    profile.clearData["unlocked"] = level + 1

  profile.saveProfile()

proc getResult*(profile: Profile, level: Natural,
                skill: Difficulty): LevelState =

  let key = "level_" & $level & "_" & $skill.int

  if key notin profile.clearData:
    return Died
  else:
    return profile.clearData[key].LevelState
