const
  SpinnerKey* = "spinner"
  DeadSpinnerKey* = "spinner_dead"
  HitboxKey* = "hitbox"
  FullGrassKey* = "grass1"
  HalfGrassKey* = "grass2"
  MowedGrassKey* = "grass3"
  RedAlienKey* = "alien_red"
  BlueAlienKey* = "alien_blue"
  CyanAlienKey* = "alien_cyan"
  GreenAlienKey* = "alien_green"
  PurpleAlienKey* = "alien_purple"
  BossAlienKey* = "boss"
  YellowBulletKey* = "bullet_yellow"
  RedBulletKey* = "bullet_red"
  BlueBulletKey* = "bullet_blue"
  CyanBulletKey* = "bullet_cyan"
  GreenBulletKey* = "bullet_green"
  PurpleBulletKey* = "bullet_purple"
  YellowBigBulletKey* = "bullet_big_yellow"
  RedBigBulletKey* = "bullet_big_red"
  BlueBigBulletKey* = "bullet_big_blue"
  CyanBigBulletKey* = "bullet_big_cyan"
  GreenBigBulletKey* = "bullet_big_green"
  PurpleBigBulletKey* = "bullet_big_purple"

  StatusBarKey* = "status_bar"
  HeartKey* = "heart"
  BronzeMedalKey* = "ribbon_bronze"
  SilverMedalKey* = "ribbon_silver"
  GoldMedalKey* = "ribbon_gold"
  BronzeIconKey* = "medal_bronze"
  SilverIconKey* = "medal_silver"
  GoldIconKey* = "medal_gold"

  MenuBackgroundKey* = "background"
  TitleTextKey* = "title_text"
  LevelSelectTextKey* = "level_select_text"
  MenuCursorKey* = "menu_cursor"
  LeftArrowKey* = "left_arrow"
  LeftSelectedKey* = "left_selected"
  RightArrowKey* = "right_arrow"
  RightSelectedKey* = "right_selected"

  EnemyFireSoundKey* = "enemy_fire"
  MowingLoopSoundKey* = "mowing_loop"
  MowingIdleSoundKey* = "mowing_idle"
  PlayerHitSoundKey* = "player_hit"
  PlayerDeathSoundKey* = "mowing_slowdown"
  FailureSoundKey* = "failure"
  VictorySoundKey* = "victory"
  BigVictorySoundKey* = "big_victory"
  TimeCountdownSoundKey* = "time_countdown"
  MenuMoveSoundKey* = "menu_move"
  MenuSelectSoundKey* = "menu_select"

  MenuMusicKey* = "menu"
  GameplayMusicKey* = "gameplay"
  BossMusicKey* = "boss"

  PlayerTag* = "player"
  PlayerHitboxTag* = "playerHitbox"
  GrassTag* = "grass"
  EnemyTag* = "enemy"
  BulletTag* = "bullet"

  GrassLayer* = 0
  PlayerLayer* = 10
  HitboxLayer* = 11
  EnemyLayer* = 20
  BigBulletLayer* = 30
  SmallBulletLayer* = 31
  StatusBarLayer* = 40
  StatusBarItemLayer* = 41
  PopupLayer* = 50

  # Magic number to indicate that this pattern has no toggle threshold
  NoToggle*: Natural = 999

  GameTitle* = "The Last Gardener"
  GameFps* = 60
  GameWidth* = 800
  GameHeight* = 600
  StatusBarHeight* = 60
  LevelWidth* = GameWidth
  LevelHeight* = GameHeight - StatusBarHeight

  SmallFontSize* = 12  # Used only by the story screen
  NormalFontSize* = 16  # Used by the story text and the status bar
  MenuItemFontSize* = 20  # Used by menu items
  PopupTitleFontSize* = 48  # Used by the pause and level end menus

type
  Difficulty* = enum
    Easy,
    Normal,
    Hard

  LevelState* = enum
    Ongoing,
    Passed,
    GrassClear,
    PerfectClear,
    Died,
    TimeUp

when not defined(release) or defined(enableDebugMode):
  let debugMode* = true
else:
  let debugMode* = false
