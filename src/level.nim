import nimgame2/[assets, entity, scene, types]
import sdl2/sdl

import consts, data, enemy, leveldefs, pattern, player, sounds

const
  TileSize = 20
  Rows = LevelHeight div TileSize
  Cols = LevelWidth div TileSize

  GrassHealth = 60  # Takes 0.33 seconds to deplete
  HalfHealth = GrassHealth div 2
  LawnHealthAtStart = GrassHealth * Rows * Cols

type
  GrassTile = ref object of Entity
    health: int
    rect: Rect

  Level* = ref object of RootObj
    number*: Natural
    skill*: Difficulty

    timeElapsed*: Natural  # In ticks
    timeLeft*: Natural

    startingLives*: Natural

    donePct*: float
    goalPct*: float

    state*: LevelState

    player: Player
    lawn: seq[GrassTile]
    enemies: seq[Enemy]

proc createLawn(level: Level) =
  for row in 0..<Rows:
    for col in 0..<Cols:
      var tile: GrassTile

      tile.new()
      tile.initEntity()
      # This is intentionally not centrified
      tile.pos = ((col * TileSize).float, (row * TileSize).float)

      tile.graphic = sprites[FullGrassKey]
      tile.layer = GrassLayer
      tile.health = GrassHealth
      tile.tags.add(GrassTag)

      # We use a Rect instead of a BoxCollider for two reasons:
      # - The default updateScene() method is O(n^2) because for each entity,
      #   it calls checkCollisions with the entire entity list, and the tag
      #   check is the last condition
      # - The original implementation checks if the tiles intersect with the
      #   player and not merely collide or share an edge, and the game was
      #   balanced around that subtle detail
      tile.rect = Rect(x: tile.pos.x.cint, y: tile.pos.y.cint,
                       w: TileSize, h: TileSize)

      level.lawn.add(tile)

proc newLevel*(def: LevelDef, skill: Difficulty): Level =
  new(result)

  result.number = def.number
  result.skill = skill
  result.startingLives = def.lives[skill.int]

  result.player = newPlayer(result.startingLives)

  result.timeElapsed = 0
  result.timeLeft = def.time[skill.int] * GameFps

  result.donePct = 0.0
  result.goalPct = def.goalPct[skill.int].float
  result.createLawn()

  result.state = Ongoing

  for e in def.enemies:
    if skill notin e.skills:
      continue

    var patterns: seq[PatternToggle]

    for p in e.patterns:
      if skill notin p.skills:
        continue

      let pattern = newPattern(cooldown = p.initialCooldown,
                               bulletKey = p.bulletKey,
                               active = p.activeAtStart,
                               onTime = p.onTime, timeArgs = p.timeArgs,
                               onFire = p.onFire, fireArgs = p.fireArgs)
      patterns.add((pattern, p.toggleAt))

    let key = e.key
    let enemy = newEnemy(graphic = sprites[key],
                         pos = e.startPos,
                         hitbox = enemyHitboxes[key],
                         speed = e.speed,
                         movement = e.movement,
                         patterns = patterns,
                         player = result.player)

    result.enemies.add(enemy)

proc passed*(level: Level): bool =
  result = level.state in [Passed, GrassClear, PerfectClear]

proc failed*(level: Level): bool =
  result = level.state in [Died, TimeUp]

proc addToScene*(level: Level, scene: Scene) =
  scene.add(level.player)
  scene.add(level.player.hitbox)

  for enemy in level.enemies:
    scene.add(enemy)

  for tile in level.lawn:
    scene.add(tile)

proc playerLives*(level: Level): Natural =
  result = level.player.lives

proc mowLawn(level: Level) =
  let damage =
    if level.player.movedThisTick:
      # Do less damage if the player is moving slower
      if level.player.focused: 2
      else: 4
    # If the player moved at all, it is rotating slightly
    elif level.player.started: 1
    # Otherwise, since the player hasn't moved yet, don't kill the tiles
    # below the player
    else: 0

  if damage == 0: return

  var
    curLawnHealth = 0
    playerRect = level.player.rect

  for tile in level.lawn:
    if hasIntersection(addr(tile.rect), addr(playerRect)):
      if tile.health > 0:
        tile.health -= damage

        if tile.health <= 0:
          tile.health = 0
          tile.graphic = sprites[MowedGrassKey]
        elif tile.health <= HalfHealth and
            tile.graphic != sprites[HalfGrassKey]:
          tile.graphic = sprites[HalfGrassKey]

    curLawnHealth += tile.health

  level.donePct = (LawnHealthAtStart - curLawnHealth) / LawnHealthAtStart * 100

proc playerClamp(level: Level) =
  const
    Margin = 16.0
    LeftBound = Margin
    RightBound = LevelWidth - Margin - 1.0
    TopBound = Margin
    BottomBound = LevelHeight - Margin - 1.0

  let
    clampX = clamp(level.player.pos.x, LeftBound, RightBound)
    clampY = clamp(level.player.pos.y, TopBound, BottomBound)

  level.player.pos = (clampX, clampY)
  # The player is not the parent of the hitbox, so we have to adjust the
  # hitbox as well
  level.player.hitbox.pos = (clampX, clampY)

proc bulletCollision(level: Level, scene: Scene) =
  if not level.player.alive:
    return

  for bullet in scene.findAll(BulletTag):
    # The bullet's collider is set to nil right after this is set to true,
    # but it won't be removed from the scene's entity list until the next tick
    if bullet.dead:
      continue
    if bullet.collider.collide(level.player.hitbox.collider):
      level.player.hitbox.onCollide(bullet)
      break  # Only one bullet should hit per tick

proc checkForEndState(level: Level) =
  if not level.player.alive:
    if level.donePct >= level.goalPct:
      level.state = Passed
    else:
      level.state = Died

  elif level.donePct == 100.0:
    if level.player.lives == level.startingLives:
      level.state = PerfectClear
    else:
      level.state = GrassClear

  elif level.timeLeft == 0:
    # Play the player death sound here to signal out of time
    sounds.playSound(PlayerDeathSoundKey)

    if level.donePct >= level.goalPct:
      level.state = Passed
    else:
      level.state = TimeUp

proc updateLevel*(level: Level, scene: Scene, elapsed: float) =
  # Don't update anything if no time passed (if the game is paused)
  if elapsed == 0.0:
    return

  # The enemies, bullets, and players have already updated their
  # state due to scene.updateScene(), and bullets that passed the
  # boundaries have been marked for deletion already
  level.playerClamp()

  for enemy in level.enemies:
    for bullet in enemy.newBullets:
      scene.add(bullet)

    enemy.newBullets.setLen(0)  # Clear the list

  if level.state == Ongoing:
    level.bulletCollision(scene)

    # This is done twice; the first time, we check to avoid the possibility
    # of dying and getting 100% at the same time
    level.checkForEndState()

  # If this is true for the first time at this point, the player died;
  # the mowing sound will be stopped in the player's damage proc
  if level.state != Ongoing:
    return

  level.mowLawn()

  # Update the enemies' knowledge of how much of the lawn has been moved
  for enemy in level.enemies:
    enemy.levelDonePct = level.donePct

  level.timeElapsed += 1
  level.timeLeft -= 1

  if level.timeLeft > 0 and level.timeLeft <= 10 * GameFps and
      level.timeLeft mod 60 == 0:
    # FIXME: the bullet sound seems to not play while this sound is playing
    sounds.playSound(TimeCountdownSoundKey)

  # Check again after the grass was damaged
  level.checkForEndState()

  # If this is the case, the player won or ran out of time
  if level.state != Ongoing:
    # Freeze the player and stop the mowing sounds
    level.player.active = false

    sounds.pauseLoop(MowingLoopSoundKey)
    sounds.pauseLoop(MowingIdleSoundKey)
