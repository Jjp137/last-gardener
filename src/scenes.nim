import os, tables

import nimgame2/[draw, entity, input, nimgame, scene, settings, textgraphic,
                 texturegraphic, types]

import consts, data, level, leveldefs, profile, sounds, statusbar, storydefs

type
  MainMenuScene = ref object of Scene
    options: seq[TextGraphic]
    curOption: Natural
    musicValue: TextGraphic
    soundValue: TextGraphic
    authorText: TextGraphic

  LevelSelectScene = ref object of Scene
    options: seq[TextGraphic]
    curOption: Natural
    curLevel: Natural
    curSkill: Natural
    maxLevel: Natural
    levelValue: TextGraphic
    skillValue: TextGraphic

  StoryScene = ref object of Scene
    text: seq[TextGraphic]
    images: seq[TextureGraphic]
    # Different from the seq above because there may be duplicates in the
    # images sequence and I'm not sure what happens if you try to free the
    # image twice
    toFree: seq[TextureGraphic]
    curSlide: Natural
    controlsText: TextGraphic
    nextScene: Scene

  GameplayScene = ref object of Scene
    level: Level
    status: StatusBar
    popup: GameplayPopup
    musicKey: string
    paused: bool
    finished: bool
    showStory: bool  # For after-level stories
    # This ties the game's logic to the rendering, which is normally not
    # a stellar idea. (It could be worse. Think about the games that try
    # to process huge dt values and fall apart as a result because the
    # physics cannot handle it.)
    #
    # Many bullet hell games, however, do slow down the gameplay if the
    # framerate drops. Sometimes, this slowdown is even intentional.
    # Regardless, this is not unusual behavior for this genre, and I would
    # rather have the game's logic slow down then have the player be hit
    # by a bullet that wasn't seen at all because a few crucial frames
    # were skipped.
    #
    # As a result, all update methods and logic procs will never use the
    # elapsed argument.
    previousFrameRendered: bool

  GameplayPopup = ref object of Entity
    scene: GameplayScene
    title: TextGraphic
    optionText: seq[TextGraphic]
    actions: seq[PopupAction]
    onEscape: PopupAction
    curOption: Natural
    medal: TextureGraphic

  PopupAction = proc(scene: GameplayScene)
  PopupOption = tuple[text: string, action: PopupAction]

# Forward declarations
proc newLevelSelectScene*(): LevelSelectScene
proc newMainMenuScene*(): MainMenuScene
proc newStoryScene*(key: string, nextScene: Scene): StoryScene
proc newGameplayScene*(level: Natural, skill: Difficulty): GameplayScene

proc drawArrows(textCenter: Coord, xOffset: float, selected: bool,
                showLeft, showRight: bool) =
  let
    leftNormal = ui[LeftArrowKey]
    leftSelected = ui[LeftSelectedKey]
    rightNormal = ui[RightArrowKey]
    rightSelected = ui[RightSelectedKey]

    # Position the left arrow based on its center-right position, and
    # position the right arrow based on its center-left position
    leftArrowCenter = (leftNormal.w.float, leftNormal.h / 2)
    rightArrowCenter = (0.0, rightNormal.h / 2)

    leftArrow = (if selected: leftSelected else: leftNormal)
    rightArrow = (if selected: rightSelected else: rightNormal)

    leftArrowPos = (textCenter.x - xOffset, textCenter.y)
    rightArrowPos = (textCenter.x + xOffset, textCenter.y)

  if showLeft:
    leftArrow.draw(pos = leftArrowPos, center = leftArrowCenter)
  if showRight:
    rightArrow.draw(pos = rightArrowPos, center = rightArrowCenter)

proc newMainMenuScene*(): MainMenuScene =
  new(result)
  result.initScene()

  result.curOption = 0
  let options = ["Start Game", "Music:", "Sound:", "Exit Game"]

  for text in options:
    var textGraphic = newTextGraphic(mainFont[MenuItemFontSize])
    textGraphic.setText(text)
    result.options.add(textGraphic)

  result.musicValue = newTextGraphic(mainFont[MenuItemFontSize])
  let musicText = if musicOn(): "On" else: "Off"
  result.musicValue.setText(musicText)

  result.soundValue = newTextGraphic(mainFont[MenuItemFontSize])
  let soundText = if soundOn(): "On" else: "Off"
  result.soundValue.setText(soundText)

  result.authorText = newTextGraphic(mainFont[NormalFontSize])
  result.authorText.setText("A game by Jjp137 and LegoBricker")

proc toggleMusic(scene: MainMenuScene) =
  sounds.toggleMusic()

  if musicOn():
    sounds.playMusic(MenuMusicKey)

  let newText = if musicOn(): "On" else: "Off"
  scene.musicValue.setText(newText)

  profile.current.musicOn = musicOn()

proc toggleSound(scene: MainMenuScene) =
  sounds.toggleSound()

  let newText = if soundOn(): "On" else: "Off"
  scene.soundValue.setText(newText)

  profile.current.soundOn = soundOn()

method show*(scene: MainMenuScene) =
  sounds.playMusic(MenuMusicKey)

method update*(scene: MainMenuScene, elapsed: float) =
  scene.updateScene(elapsed)

  if ScancodeReturn.pressed:
    case scene.curOption:
      of 0: game.scene = newLevelSelectScene()
      of 1: scene.toggleMusic()
      of 2: scene.toggleSound()
      of 3: gameRunning = false
      else: assert(false)
    sounds.playSound(MenuSelectSoundKey)
  elif ScancodeUp.pressed and scene.curOption != 0:
    sounds.playSound(MenuMoveSoundKey)
    dec scene.curOption
  elif ScancodeDown.pressed and scene.curOption != scene.options.len - 1:
    sounds.playSound(MenuMoveSoundKey)
    inc scene.curOption
  elif ScancodeLeft.pressed or ScancodeRight.pressed:
    case scene.curOption:
      of 0, 3: discard
      of 1:
        scene.toggleMusic()
        sounds.playSound(MenuMoveSoundKey)
      of 2:
        scene.toggleSound()
        sounds.playSound(MenuMoveSoundKey)
      else: assert(false)
  elif ScancodeEscape.pressed:
    gameRunning = false

method render*(scene: MainMenuScene) =
  scene.renderScene()
  sounds.tick()

  let
    background = ui[MenuBackgroundKey]
    titleText = ui[TitleTextKey]
    cursor = ui[MenuCursorKey]

    transparentBlack = Color(r: 0, g: 0, b: 0, a: 179)

    menuTopLeft = (484.0, 364.0)
    menuBottomRight = (784.0, 574.0)

    authorTopLeft = (506.0, 576.0)
    authorBottomRight = (784.0, 598.0)

    textTopLeft = authorTopLeft + (2.0, 2.0)

  background.draw(pos = (0.0, 0.0))
  # The images for the text are basically transparent layers separated from the
  # background, and they are not cropped, so they are still 800x600
  titleText.draw(pos = (0.0, 0.0))

  discard box(menuTopLeft, menuBottomRight, transparentBlack)

  discard box(authorTopLeft, authorBottomRight, transparentBlack)
  scene.authorText.draw(pos = textTopLeft)

  for i, option in scene.options:
    let
      textX = 540.0
      textY = 390.0 + 50.0 * i.float
      # Position each text item based on its center-left position
      textCenter = (0.0, option.h / 2)

      cursorX = textX - 10
      # Position the cursor based on its center-right position
      cursorCenter = (cursor.w.float, cursor.h / 2)

      selected = (i == scene.curOption)
      color = (if selected: ColorYellow else: ColorWhite)

    option.color = color
    option.draw(pos = (textX, textY), center = textCenter)

    if selected:
      cursor.draw(pos = (cursorX, textY), center = cursorCenter)

    var valueToDraw: TextGraphic = nil

    if i == 1:  # Music
      valueToDraw = scene.musicValue
    elif i == 2:  # Sounds
      valueToDraw = scene.soundValue

    if valueToDraw != nil:
      const
        valueX = 690.0  # For "On" and "Off" text
        arrowOffset = 30.0  # Distance of the arrows from the text's center
      let
        valuePos = (valueX, textY)
        valueCenter = (valueToDraw.w / 2, valueToDraw.h / 2)

      drawArrows(valuePos, arrowOffset, selected, true, true)
      valueToDraw.color = color
      valueToDraw.draw(pos = valuePos, center = valueCenter)

proc newLevelSelectScene*(): LevelSelectScene =
  new(result)
  result.initScene()

  result.curOption = 0
  result.curLevel = profile.current.lastPlayed.number
  result.curSkill = profile.current.lastPlayed.skill.Natural

  let options = ["Level:", "Difficulty:", "Start Level", "Back to Main Menu"]
  for text in options:
    var textGraphic = newTextGraphic(mainFont[MenuItemFontSize])
    textGraphic.setText(text)
    result.options.add(textGraphic)

  result.levelValue = newTextGraphic(mainFont[MenuItemFontSize])
  result.levelValue.setText($result.curLevel)

  const skillNames = ["Easy", "Normal", "Hard"]
  result.skillValue = newTextGraphic(mainFont[MenuItemFontSize])
  result.skillValue.setText(skillNames[result.curSkill])

  # If the highest unlocked level happens to be 11, it'll be 10 instead
  result.maxLevel = min(profile.current.clearData["unlocked"], levelEntries.len)

  # Just in case...
  result.curLevel = min(result.curLevel, result.maxLevel)

proc adjustLevel(scene: LevelSelectScene, direction: int) =
  if direction == -1 and scene.curLevel != 1:
    sounds.playSound(MenuMoveSoundKey)
    dec scene.curLevel
  elif direction == +1 and scene.curLevel != scene.maxLevel:
    sounds.playSound(MenuMoveSoundKey)
    inc scene.curLevel

  scene.levelValue.setText($scene.curLevel)

proc adjustDifficulty(scene: LevelSelectScene, direction: int) =
  if direction == -1 and scene.curSkill != 0:
    sounds.playSound(MenuMoveSoundKey)
    dec scene.curSkill
  elif direction == +1 and scene.curSkill != 2:
    sounds.playSound(MenuMoveSoundKey)
    inc scene.curSkill

  const skillNames = ["Easy", "Normal", "Hard"]
  scene.skillValue.setText(skillNames[scene.curSkill])

proc startLevel(scene: LevelSelectScene) =
  let
    levelData = levelEntries[scene.curLevel]
    storyKey = levelData.storyBefore
    gameplay = newGameplayScene(scene.curLevel, scene.curSkill.Difficulty)

  if storyKey != "":
    game.scene = newStoryScene(storyKey, gameplay)
  else:
    game.scene = gameplay

  profile.current.lastPlayed.number = scene.curLevel
  profile.current.lastPlayed.skill = scene.curSkill.Difficulty

method show*(scene: LevelSelectScene) =
  sounds.playMusic(MenuMusicKey)

method update*(scene: LevelSelectScene, elapsed: float) =
  scene.updateScene(elapsed)

  # Copied and pasted from the MainMenuScene, of course...
  if ScancodeReturn.pressed:
    case scene.curOption:
      of 0, 1: discard  # These aren't toggles so it makes no sense
      of 2:
        sounds.playSound(MenuSelectSoundKey)
        scene.startLevel()
      of 3:
        sounds.playSound(MenuSelectSoundKey)
        game.scene = newMainMenuScene()
      else:
        assert(false)
  elif ScancodeUp.pressed and scene.curOption != 0:
    sounds.playSound(MenuMoveSoundKey)
    dec scene.curOption
  elif ScancodeDown.pressed and scene.curOption != scene.options.len - 1:
    sounds.playSound(MenuMoveSoundKey)
    inc scene.curOption
  elif ScancodeLeft.pressed:
    case scene.curOption:
      of 0: scene.adjustLevel(-1)
      of 1: scene.adjustDifficulty(-1)
      of 2, 3: discard
      else: assert(false)
  elif ScancodeRight.pressed:
    case scene.curOption:
      of 0: scene.adjustLevel(+1)
      of 1: scene.adjustDifficulty(+1)
      of 2, 3: discard
      else: assert(false)
  elif ScancodeEscape.pressed:
    sounds.playSound(MenuSelectSoundKey)
    game.scene = newMainMenuScene()

method render*(scene: LevelSelectScene) =
  scene.renderScene()
  sounds.tick()

  # Also largely copied and pasted...
  let
    background = ui[MenuBackgroundKey]
    levelSelectText = ui[LevelSelectTextKey]
    cursor = ui[MenuCursorKey]

    transparentBlack = Color(r: 0, g: 0, b: 0, a: 179)
    topLeftPos = (446.0, 363.0)
    bottomRightPos = (784.0, 573.0)

  background.draw(pos = (0.0, 0.0))
  # The images for the text are basically transparent layers separated from the
  # background, and they are not cropped, so they are still 800x600
  levelSelectText.draw(pos = (0.0, 0.0))

  discard box(topLeftPos, bottomRightPos, transparentBlack)

  for i, option in scene.options:
    let
      textX = 482.0
      textY = 393.0 + 50.0 * i.float
      # Position each text item based on its center-left position
      textCenter = (0.0, option.h / 2)

      cursorX = textX - 10
      # Position the cursor based on its center-right position
      cursorCenter = (cursor.w.float, cursor.h / 2)

      selected = (i == scene.curOption)
      color = (if selected: ColorYellow else: ColorWhite)

    option.color = color
    option.draw(pos = (textX, textY), center = textCenter)

    if selected:
      cursor.draw(pos = (cursorX, textY), center = cursorCenter)

    var
      valueToDraw: TextGraphic = nil
      valueX = 0.0
      arrowOffset = 0.0
      showLeft = false
      showRight = false

    if i == 0:  # Level number
      valueToDraw = scene.levelValue
      valueX = 607.0  # Original: 617.0
      arrowOffset = 25.0  # Original: 30.0
      showLeft = (scene.curLevel != 1)
      showRight = (scene.curLevel != scene.maxLevel)
    elif i == 1:  # Difficulty
      valueToDraw = scene.skillValue
      valueX = 677.0   # Original: 707.0
      arrowOffset = 50.0  # Original: 60.0
      showLeft = (scene.curSkill != 0)
      showRight = (scene.curSkill != 2)

    if valueToDraw != nil:
      let
        valuePos = (valueX, textY)
        valueCenter = (valueToDraw.w / 2, valueToDraw.h / 2)

      drawArrows(valuePos, arrowOffset, selected, showLeft, showRight)
      valueToDraw.color = color
      valueToDraw.draw(pos = valuePos, center = valueCenter)

    if i == 0:  # The level select option
      const medalTable = {
        Passed: BronzeIconKey,
        GrassClear: SilverIconKey,
        PerfectClear: GoldIconKey
      }.toTable

      let levelResult = profile.current.getResult(scene.curLevel,
                                                  scene.curSkill.Difficulty)
      if levelResult in medalTable:
        let
          medalImage = sprites[medalTable[levelResult]]
          medalPos = (textX + 220.0, textY)
          medalCenter = (medalImage.w.float / 2, medalImage.h.float / 2)

        medalImage.draw(pos = medalPos, center = medalCenter)

proc newStoryScene*(key: string, nextScene: Scene): StoryScene =
  new(result)
  result.initScene()

  result.controlsText = newTextGraphic(mainFont[SmallFontSize])
  result.controlsText.setText("Space or Enter to advance, Escape to skip")

  result.curSlide = 0

  let storyData = slides[key]
  for item in storyData:
    let newText = newTextGraphic(mainFont[NormalFontSize])
    # FIXME: the font isn't fixed-width so this isn't optimal
    newText.setText(item.text, 90)
    result.text.add(newText)

    # We load them here so that we can free them after the story screen
    # has ended and save memory that way
    if item.image != NoImage:
      let filename = "data" / "story" / item.image
      echo "Loading: " & filename

      let newImage = newTextureGraphic(filename)
      result.images.add(newImage)
      result.toFree.add(newImage)
    else:
      # Find the previous slide with an image and use that
      # This assumes that the first slide always has an image
      var newImage: TextureGraphic = nil
      for i in countdown(result.images.len - 1, 0):
        if result.images[i] != nil:
          newImage = result.images[i]
          break

      # Can be nil if images[0..i] is all nil somehow
      result.images.add(newImage)

  result.nextScene = nextScene

method show*(scene: StoryScene) =
  sounds.playMusic(MenuMusicKey)

method hide*(scene: StoryScene) =
  for image in scene.toFree:
    image.free()

method update*(scene: StoryScene, elapsed: float) =
  scene.updateScene(elapsed)

  if ScancodeSpace.pressed or ScancodeReturn.pressed:
    sounds.playSound(MenuSelectSoundKey)

    if scene.curSlide == scene.images.len - 1:
      game.scene = scene.nextScene
    else:
      inc scene.curSlide
  elif ScancodeEscape.pressed:
    sounds.playSound(MenuSelectSoundKey)
    game.scene = scene.nextScene

method render*(scene: StoryScene) =
  scene.renderScene()
  sounds.tick()

  let
    textPos = (10.0, 510.0)  # Original: y = 75 (525 with normal axis)

    transparentBlack = Color(r: 0, g: 0, b: 0, a: 179)
    topLeftPos = (0.0, 500.0)
    bottomRightPos = (GameWidth, GameHeight)

    controlsPos = (790.0, 595.0)
    # Anchor this text on the lower-right
    controlsCenter = (scene.controlsText.w.float, scene.controlsText.h.float)

    index = scene.curSlide

  scene.images[index].draw(pos = (0, 0))

  discard box(topLeftPos, bottomRightPos, transparentBlack)

  scene.text[index].draw(pos = textPos)
  scene.controlsText.draw(pos = controlsPos, center = controlsCenter)

proc newGameplayPopup(scene: GameplayScene, title: string, titleColor: Color,
                      options: seq[PopupOption], defaultOpt: Natural,
                      onEscape: PopupAction,
                      medal: TextureGraphic): GameplayPopup =
  new(result)
  result.initEntity()

  result.layer = PopupLayer

  result.scene = scene
  result.curOption = defaultOpt
  result.onEscape = onEscape

  for option in options:
    let text = newTextGraphic(mainFont[MenuItemFontSize])
    text.setText(option.text)

    result.optionText.add(text)
    result.actions.add(option.action)

  assert result.optionText.len == result.actions.len

  result.title = newTextGraphic(mainFont[PopupTitleFontSize])
  result.title.setText(title)
  result.title.color = titleColor

  result.medal = medal

method update(popup: GameplayPopup, elapsed: float) =
  popup.updateEntity(elapsed)

  if ScancodeUp.pressed and popup.curOption != 0:
    sounds.playSound(MenuMoveSoundKey)
    dec popup.curOption
  elif ScancodeDown.pressed and popup.curOption != popup.actions.len - 1:
    sounds.playSound(MenuMoveSoundKey)
    inc popup.curOption
  elif ScancodeReturn.pressed:
    sounds.playSound(MenuSelectSoundKey)
    popup.actions[popup.curOption](popup.scene)
    # Help the GC and break the cycle betwen the scene and the popup
    # For some reason, Nim's default GC segfaults eventually without this...
    # The popup disappears afterward so it's fine
    popup.scene = nil
  elif ScancodeEscape.pressed:
    clearPressed(ScancodeEscape)  # Otherwise, the pause menu will appear again
    popup.onEscape(popup.scene)
    # Same here
    popup.scene = nil

method render(popup: GameplayPopup) =
  popup.renderEntity()

  let
    topLeftPos = (150.0, 170.0)
    bottomRightPos = (650.0, 370.0)
    transparentBlack = Color(r: 0, g: 0, b: 0, a: 179)

  discard box(topLeftPos, bottomRightPos, transparentBlack)

  let
    popupCenter = (x: 400.0, y: 270.0)
    titlePos = (400.0, 220.0)
    titleCenter = (popup.title.w / 2, popup.title.h / 2)

  popup.title.draw(pos = titlePos, center = titleCenter)

  for i, option in popup.optionText:
    let
      # Divide the available space based on the number of options
      # present and determine the y-coordinate for each option from there
      yInterval = 90.0 / popup.optionText.len.float

      textX = popupCenter.x - 80.0
      textY = popupCenter.y + yInterval * i.float
      # Position each text item based on its center-left position
      textCenter = (0.0, option.h / 2)

      cursor = ui[MenuCursorKey]
      cursorX = popupCenter.x - 90.0
      # Position the cursor based on its center-right position
      cursorCenter = (cursor.w.float, cursor.h / 2)

      selected = (popup.curOption == i)
      color = (if selected: ColorYellow else: ColorWhite)

    option.color = color
    option.draw(pos = (textX, textY), center = textCenter)

    if selected:
      cursor.draw(pos = (cursorX, textY), center = cursorCenter)

  if popup.medal != nil:
    let
      medalPos = (popupCenter.x + 150, popupCenter.y - 10)
      # Position it from the center-top
      medalCenter = (popup.medal.w / 2, 0.0)

    popup.medal.draw(pos = medalPos, center = medalCenter)

proc newGameplayScene*(level: Natural, skill: Difficulty): GameplayScene =
  new(result)
  result.initScene()

  result.level = newLevel(levelEntries[level], skill)
  result.level.addToScene(result)

  result.status = newStatusBar(result.level)
  result.add(result.status)

  result.popup = nil

  result.paused = false
  result.finished = false
  result.previousFrameRendered = true

  # lol hardcoding
  result.musicKey =
    if result.level.number == 10: BossMusicKey
    else: GameplayMusicKey

proc cleanup(scene: GameplayScene) =
  # Help the GC and break the cycle betwen the scene and the popup
  # For some reason, Nim's default GC segfaults eventually without this...
  if scene.popup != nil:
    discard scene.del(scene.popup)
    scene.popup = nil

  # Colliders and entities create cycles with each other so remove
  # colliders from all entities that are still present, especially bullets
  for entity in scene.entities:
    entity.collider = nil

proc restart(scene: GameplayScene) =
  var gameplay = newGameplayScene(scene.level.number, scene.level.skill)
  # If the player previously passed the level and wants to retry, make
  # sure to still show the post-level cutscene if the player quits
  gameplay.showStory = scene.showStory

  game.scene = gameplay
  scene.cleanup()

proc goBack(scene: GameplayScene) =
  let storyKey = levelEntries[scene.level.number].storyClear

  if scene.showStory and storyKey != "":
    game.scene = newStoryScene(storyKey, newLevelSelectScene())
  else:
    game.scene = newLevelSelectScene()

  scene.cleanup()

proc nextLevel(scene: GameplayScene) =
  if scene.level.number == levelEntries.len:
    scene.goBack()
    return

  let gameplay = newGameplayScene(scene.level.number + 1, scene.level.skill)

  var
    postStory: StoryScene = nil  # The post-level story of the current level
    postKey = levelEntries[scene.level.number].storyClear
    preStory: StoryScene = nil  # The pre-level story of the next level
    preKey = levelEntries[scene.level.number + 1].storyBefore

  # First check if there is a pre-level cutscene that needs to be played;
  # if so, attach the gameplay scene to that
  if preKey != "":
    preStory = newStoryScene(preKey, gameplay)

  if postKey != "":
    # Check if there is a pre-level cutscene that needs to be played after
    # this level's post-level cutscene
    if preStory != nil:
      postStory = newStoryScene(postKey, preStory)
    # If not, attach the gameplay to the post-level cutscene
    else:
      postStory = newStoryScene(postKey, gameplay)

  game.scene =
    if postStory != nil: postStory
    elif preStory != nil: preStory
    else: gameplay

  scene.cleanup()

proc unpause(scene: GameplayScene) =
  scene.paused = false

  discard scene.del(scene.popup)
  scene.popup = nil

  sounds.playMusic(scene.musicKey)

proc pause(scene: GameplayScene) =
  scene.paused = true

  # Stop the mowing sounds
  sounds.pauseLoop(MowingLoopSoundKey)
  sounds.pauseLoop(MowingIdleSoundKey)

  sounds.pauseMusic()

  let options = @[(text: "Continue", action: PopupAction(unpause)),
                  (text: "Retry", action: PopupAction(restart)),
                  (text: "Level Select", action: PopupAction(goBack))]

  scene.popup = newGameplayPopup(scene, "Paused", ColorWhite, options, 0,
                                 unpause, nil)
  scene.add(scene.popup)

proc handleEndState(scene: GameplayScene) =
  const popupParams = {
    Passed: ("Level Clear!", ColorLightBlue, BronzeMedalKey),
    GrassClear: ("Fully Mowed!", ColorSilver, SilverMedalKey),
    PerfectClear: ("Perfect!", ColorGold, GoldMedalKey),
    Died: ("Level Failed!", ColorRed, ""),
    TimeUp: ("Time Up!", ColorRed, "")
  }.toTable

  let
    params = popupParams[scene.level.state]
    title = params[0]
    color = params[1]
    medal = (if params[2] != "": sprites[params[2]] else: nil)

  var options = @[(text: "Retry", action: PopupAction(restart)),
                  (text: "Level Select", action: PopupAction(goBack))]

  if scene.level.passed:
    options.insert((text: "Next Level", action: PopupAction(nextLevel)))

  scene.popup = newGameplayPopup(scene, title, color, options, 0, goBack, medal)
  scene.add(scene.popup)

  sounds.pauseMusic()

  if scene.level.failed:
    sounds.playSound(FailureSoundKey)
  elif scene.level.state in [Passed, GrassClear]:
    sounds.playSound(VictorySoundKey)
  elif scene.level.state == PerfectClear:
    sounds.playSound(BigVictorySoundKey)
  else:
    assert(false)

  if scene.level.passed:
    # Even after you retry and then quit, still show the story
    scene.showStory = true

    profile.current.updateResult(scene.level.number, scene.level.skill,
                                 scene.level.state)

method show*(scene: GameplayScene) =
  sounds.playMusic(scene.musicKey, true)

method update*(scene: GameplayScene, elapsed: float) =
  # Ensure that we only run one tick for each frame rendered.
  if scene.previousFrameRendered:
    let actualElapsed = if scene.paused: 0.0 else: elapsed

    scene.updateScene(actualElapsed)
    scene.level.updateLevel(scene, actualElapsed)

    # Crappy hack to change the music once the battle is over
    if scene.level.number == 10 and not scene.paused and
        scene.level.timeLeft < 875 * GameFps and not scene.finished:
      scene.musicKey = GameplayMusicKey
      sounds.playMusic(scene.musicKey)

    if scene.level.state != Ongoing and not scene.finished:
      scene.finished = true
      scene.handleEndState()

    if ScancodeEscape.pressed:
      scene.pause()

    scene.previousFrameRendered = false

  if debugMode:
    if MouseButton.left.pressed:
      echo mouse
    if ScancodeF2.pressed:
      showInfo = not showInfo
    if ScancodeF3.pressed:
      colliderOutline = not colliderOutline
    # Temporary restart shortcut
    if ScancodeR.pressed:
      if scene.popup != nil:
        scene.popup.scene = nil
        scene.popup = nil
      scene.restart()
    if ScancodeS.pressed:
      fpsLimit = 30
    if ScancodeS.released:
      fpsLimit = 0  # No limit

method render*(scene: GameplayScene) =
  scene.renderScene()
  sounds.tick()

  scene.previousFrameRendered = true

  if debugMode and colliderOutline:
    # If an entity's colliderEnabled field is false, then toggling
    # colliderOutline has no effect on that entity, but because we're
    # checking bullet collisions by ourselves in the Level object, seeing
    # the hitboxes related to bullet collision is still helpful
    for bullet in scene.findAll(BulletTag):
      if bullet.collider != nil:
        bullet.collider.render()

    let playerHitbox = scene.find(PlayerHitboxTag)
    if playerHitbox != nil:  # Can occur the frame after a restart
      playerHitbox.collider.render()
