import tables

import nimgame2/[assets, entity, texturegraphic, types]
import sdl2/sdl

import consts

const
  SmallHitbox: Dim = (8.0, 8.0)
  BigHitbox: Dim = (32.0, 32.0)
  Margin = 20

type
  Bullet* = ref object of Entity
    hitboxDim: Dim

let bulletHitboxes*: Table[string, Dim] = {
  YellowBulletKey: SmallHitbox,
  RedBulletKey: SmallHitbox,
  BlueBulletKey: SmallHitbox,
  CyanBulletKey: SmallHitbox,
  GreenBulletKey: SmallHitbox,
  PurpleBulletKey: SmallHitbox,
  YellowBigBulletKey: BigHitbox,
  RedBigBulletKey: BigHitbox,
  BlueBigBulletKey: BigHitbox,
  CyanBigBulletKey: BigHitbox,
  GreenBigBulletKey: BigHitbox,
  PurpleBigBulletKey: BigHitbox,
}.toTable

proc newBullet*(graphic: TextureGraphic, hitbox: Dim; pos, vel: Coord): Bullet =
  new(result)
  result.initEntity()

  result.graphic = graphic
  result.pos = pos
  result.vel = vel
  result.centrify()

  result.layer = (if hitbox == BigHitbox: BigBulletLayer else: SmallBulletLayer)

  result.tags.add(BulletTag)

  result.collider = result.newBoxCollider((0.0, 0.0), hitbox)
  result.collider.tags.add(PlayerHitboxTag)
  # We do it ourselves in the Level class for performance reasons
  result.colliderEnabled = false

  # Keep track of the hitbox's size for Rect creation
  result.hitboxDim = hitbox

proc rect*(bullet: Bullet): Rect =
  let topLeft = (x: bullet.pos.x - (bullet.hitboxDim.w / 2),
                 y: bullet.pos.y - (bullet.hitboxDim.h / 2))

  result = Rect(x: topLeft.x.cint, y: topLeft.y.cint,
                w: bullet.hitboxDim.w.cint, h: bullet.hitboxDim.h.cint)

method update*(bullet: Bullet, elapsed: float) =
  # Don't update anything if no time passed (if the game is paused)
  if elapsed == 0.0:
    return

  bullet.pos += bullet.vel

  # Remove bullets that are out of bounds
  if bullet.pos.x < -Margin or bullet.pos.x > LevelWidth + Margin or
      bullet.pos.y < -Margin or bullet.pos.y > LevelHeight + Margin:
    bullet.dead = true
    # This allows the GC to free the bullet's memory because otherwise
    # there is a cycle due to the collider having a parent field pointing
    # to this bullet
    bullet.collider = nil
