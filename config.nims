import os

task makeDist, "make a folder that's ready to be compressed":
  const gameDir = "the-last-gardener"

  mkDir(gameDir)

  cpDir("data", gameDir / "data")
  cpDir("libs", gameDir / "libs")
  cpFile("gardener.sh", gameDir / "gardener.sh")
  cpFile("LICENSE.txt", gameDir / "LICENSE.txt")
  cpFile("README.rst", gameDir / "README.rst")

  when not defined(release):
    echo "This is not a release build!"
    echo "Press Enter to confirm building a non-release build."
    discard readLineFromStdin()

  const shPath = gameDir / "gardener.sh"
  exec("chmod +x " & shPath)

  switch("out", gameDir / "gardener")
  setCommand("c")
